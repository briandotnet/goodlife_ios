//
//  WebViewController.h
//  goodlife
//
//  Created by Brian Ge on 12-02-04.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCAppUtils.h"
#import "UIBarButtonItem.h"
@interface WebViewController : UIViewController <UIWebViewDelegate>{

    UIWebView *webview;
    UIActivityIndicatorView *loading_indicator;
    NSURL *url;
}    

@property (nonatomic, retain) IBOutlet UIWebView *webview;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loading_indicator;
@property (nonatomic, retain) NSURL *url;
@end
