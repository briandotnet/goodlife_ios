//
//  ClubDetailsViewController.h
//  goodlife
//
//  Created by Brian Ge on 12-01-28.
//  Copyright (c) 2012 T4G. All rights reserved.
//
//HELLO

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Club.h"
#import "Schedule.h"
#import "UIBarButtonItem.h"
#import "ScheduleDetailsViewController.h"
#import "WebViewController.h"
#import "FeedbackViewController.h"
#import "SCAppUtils.h"
#import "TestFlight.h"

@interface ClubDetailsViewController : UIViewController<MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>{
    UILabel *club_name_label;
    UILabel *club_address_label;
    MKMapView *club_map_view;
    UITableView *club_details_table_live;
    Club *club;
    CLLocationCoordinate2D currentLocationCoordinate;
    
    UITableViewCell *club_details_cell;
}
@property (nonatomic, retain) IBOutlet UILabel *club_name_label;
@property (nonatomic, retain) IBOutlet UILabel *club_address_label;
@property (nonatomic, retain) IBOutlet MKMapView *club_map_view;
@property (nonatomic, retain) IBOutlet UITableView *club_details_table_live;
@property (nonatomic, retain) Club* club;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocationCoordinate;
@property (nonatomic, assign) IBOutlet UITableViewCell *club_details_cell;


@end
