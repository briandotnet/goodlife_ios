//
//  ClubDetailsViewController.m
//  goodlife
//
//  Created by Brian Ge on 12-01-28.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "ClubDetailsViewController.h"
@implementation ClubDetailsViewController

@synthesize club_name_label, club_address_label, club_map_view, club_details_table_live, club_details_cell;
@synthesize club, currentLocationCoordinate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    [TestFlight passCheckpoint:@"memory warning at club details view"];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImage *backImage = [UIImage imageNamed:@"back"];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithImage:backImage title:@"" target:self action:@selector(back)] autorelease];
    self.title = @"Club Details";
    MKCoordinateRegion region;
    region.center = CLLocationCoordinate2DMake(self.club.latitude, self.club.longitude);  
    
    MKCoordinateSpan span; 
    span.latitudeDelta  = 0.01; // Change these values to change the zoom
    span.longitudeDelta = 0.01; 
    region.span = span;
    
    [self.club_map_view setRegion:region animated:NO];
    [self.club_map_view addAnnotation:self.club];
    
    self.club_name_label.text = club.name;
    self.club_address_label.text = [NSString stringWithFormat:@"%@, %@", club.subtitle, club.province];

}

//-(void) viewWillAppear:(BOOL)animated{
//    if ([[UINavigationBar class]respondsToSelector:@selector(appearance)]) {
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"club_details_nav.png"] forBarMetrics:UIBarMetricsDefault];
//    }
//}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    club_name_label = nil; 
    club_address_label = nil;
    club_map_view = nil;
    club_details_table_live = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Map 

- (MKAnnotationView *) mapView:(MKMapView *) mapView viewForAnnotation:(id ) annotation {
    MKPinAnnotationView *pin;
    if([annotation isKindOfClass:[Club class]]) {
        
        NSString *club_name = ((Club*)annotation).name;
        pin = [[[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: club_name] autorelease];
        pin.pinColor = MKPinAnnotationColorRed;
        pin.animatesDrop = NO;
        pin.canShowCallout = NO;
    }
    else{
        pin = [[[MKPinAnnotationView alloc] init] autorelease];
        pin.pinColor = MKPinAnnotationColorPurple;
    }
    return pin;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2; // club contact info, schedules
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0: // club contact info: directions, phone, email, feedback
            return 5;
            break;
        case 1: // schedules
            return self.club.schedules.count;
            break;
        default:
            return 1;
    }
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *header_view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 288, 30)] autorelease];
    header_view.backgroundColor = [UIColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:0.8f];
    
    UILabel *title_label = [[[UILabel alloc] initWithFrame:CGRectMake(10, 0, 268, 30)] autorelease];
    switch (section) {
        case 0:
            title_label.text = @"Club Info";
            break;
        case 1:
            title_label.text = @"Group Exercise Schedules";
            break;
        default:
            title_label.text = @"";
            break;
    }
    title_label.textAlignment = UITextAlignmentLeft;
    title_label.backgroundColor = [UIColor clearColor];
    title_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    title_label.textColor = [UIColor whiteColor];
    title_label.shadowColor = [UIColor lightGrayColor];
    title_label.shadowOffset = CGSizeMake(0, 1);
    
    [header_view addSubview:title_label];
    
    return header_view;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    switch (section) {
        case 0:
            return @"Club Info";
            break;
        case 1:
            return @"Group Exercise Schedules";
        default:
            return @"";
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"club_details_cell"];
    if(cell == nil){
        [[NSBundle mainBundle] loadNibNamed:@"clubDetailsCell" owner:self options:nil];
        cell = club_details_cell;
        self.club_details_cell = nil;
    }
    
    UIImageView *icon;
    icon = (UIImageView *) [cell viewWithTag:1];
    
    UILabel *label;
    label = (UILabel *) [cell viewWithTag:2];

    // Configure the cell...
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    icon.image = [UIImage imageNamed:@"directions.png"];
                    label.text = @"Get Directions";
                    break;
                case 1:
                    icon.image = [UIImage imageNamed:@"phone.png"];
                    label.text = club.phone;
                    break;
                case 2:
                    icon.image = [UIImage imageNamed:@"email.png"];
                    label.text = club.email;
                    [label setLineBreakMode:UILineBreakModeCharacterWrap];
                    break;
                case 3:
                    icon.image = [UIImage imageNamed:@"website.png"];
                    label.text = @"View in Browser";
                    break;
                case 4:
                    icon.image = [UIImage imageNamed:@"feedback.png"];
                    label.text = @"Submit Feedback";
                    break;
                default:
                    break;
            }
            break;
        case 1:
            icon.image = [UIImage imageNamed:@"calendars.png"];
            label.text = ((Schedule *)[self.club.schedules objectAtIndex:indexPath.row]).name;
            break;
        default:
            break;
    }

    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* url;
    UIDevice *device = [UIDevice currentDevice];
    UINavigationController *nav;
    WebViewController *webview;
    ScheduleDetailsViewController *scheduleDetailsController;
    FeedbackViewController *feedbackController;
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    
                    url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%f,%f&daddr=%@",
                           self.currentLocationCoordinate.latitude, self.currentLocationCoordinate.longitude,
                           [club.subtitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
                    break;
                case 1:
                    if ([[device model] isEqualToString:@"iPhone"] ) {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat: @"Call %@?",[club.phone stringByReplacingOccurrencesOfString:@" " withString:@""]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
                        [alert show];
                        alert.tag = 2;
                        [alert release];
                    } else {
                        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat: @"Sorry, you device can not make phone calls but here is the number: %@", club.phone ] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [Notpermitted show];
                        [Notpermitted release];
                    }
                    break;
                case 2:
                    if([MFMailComposeViewController canSendMail]){
                        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
                        picker.mailComposeDelegate = self;
                        [picker setToRecipients:[NSArray arrayWithObject:club.email]];
                        [self presentModalViewController:picker animated:YES];
                        [picker release];

                    }
                    break;
                case 3:

                    webview = [[[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil] autorelease];
                    webview.url = [NSURL URLWithString:club.url];
                    webview.title = club.name;
                    nav = [[[UINavigationController alloc] initWithRootViewController:webview] autorelease];
                    [self.navigationController presentModalViewController:nav animated:YES];
                    
                    break;
                case 4:
                    feedbackController = [[[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil] autorelease];
                    feedbackController.club = self.club;
                    nav = [[[UINavigationController alloc] initWithRootViewController:feedbackController] autorelease];
                    [self.navigationController presentModalViewController:nav animated:YES];
                    break;
                default:
                    break;
            }

            break;
        case 1:
            // push on schedule view
            scheduleDetailsController = [[[ScheduleDetailsViewController alloc] initWithNibName:@"ScheduleDetailsViewController" bundle:nil] autorelease];
            scheduleDetailsController.schedule = [club.schedules objectAtIndex:indexPath.row];
            scheduleDetailsController.club = club;
            [self.navigationController pushViewController:scheduleDetailsController animated:YES];
            break;
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];

}



#pragma mark - UIAlertViewDelegate

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case 1: // phone
            if (buttonIndex == 1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [club.phone stringByReplacingOccurrencesOfString:@" " withString:@""]]]];
            }
            break;
        default:
            break;
    }
}

#pragma mark - MFMailComposeViewControllerDelegate
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's sent!");
    }
    [self dismissModalViewControllerAnimated:YES];
}
@end
