//
//  ScheduleDetailsViewController.h
//  goodlife
//
//  Created by Brian Ge on 12-02-02.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "GroupClass.h"
#import "UIBarButtonItem.h"
#import "Schedule.h"
#import "GroupClass.h"
#import "Club.h"
#import "ModelObjectFactory.h"
#import "SCAppUtils.h"
#import "ASIHTTPRequest.h"
#import "ServiceManager.h"
#import "ScheduleDetailsViewController.h"
#import "WebViewController.h"
#import "MBProgressHUD.h"

@interface ScheduleDetailsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate, ASIHTTPRequestDelegate, UIAlertViewDelegate, MBProgressHUDDelegate>{
    UILabel *club_name_label;
    MKMapView *club_map_view;
    UILabel *schedule_name_label;
    UILabel *effective_dates_label;
    UITableView *classes_table_view;
    UITableViewCell *class_cell;
    MBProgressHUD *HUD;
    
    Schedule *schedule;
    Club *club;
    GroupClass *selected_class;
    ServiceManager *serviceManager;
    
}

@property (nonatomic, retain) IBOutlet UILabel *club_name_label;
@property (nonatomic, retain) IBOutlet MKMapView *club_map_view;
@property (nonatomic, retain) IBOutlet UILabel *schedule_name_label;
@property (nonatomic, retain) IBOutlet UILabel *effective_dates_label;
@property (nonatomic, retain) IBOutlet UITableView *classes_table_view;
@property (nonatomic, assign) IBOutlet UITableViewCell *class_cell;
@property (nonatomic, retain) Schedule *schedule;
@property (nonatomic, retain) Club *club;
@property (nonatomic, retain) GroupClass *selected_calss;
@property (nonatomic, retain) ServiceManager *serviceManager;

- (void) showHud;
-(IBAction) viewInBrowser;
- (void) addClassToCalendar:(id)sender event:(id)event;
-(EKCalendar *) getCalendar;
-(void) createEventForClass: (GroupClass *)class inCalendar:(EKCalendar *)calendar repeatWeekly:(BOOL)repeatWeekly;

@end
