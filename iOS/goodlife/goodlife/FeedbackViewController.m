//
//  FeedbackViewController.m
//  goodlife
//
//  Created by Brian Ge on 12-02-05.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "FeedbackViewController.h"

@implementation FeedbackViewController

@synthesize club, schedule, serviceManager, issues, suggestion, name, email, container_view, content_view;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.serviceManager = [ServiceManager sharedManager];

    // Do any additional setup after loading the view from its nib.
    NSMutableDictionary *dict = [[[NSMutableDictionary alloc] init ] autorelease];
    if (self.club) {
        [dict setValue:self.club forKey:@"club"];
    }
    if(self.schedule){
        [dict setValue:self.schedule forKey:@"schedule"];
    }
    [FlurryAnalytics logEvent:@"FeedbackViewController loaded" withParameters:dict];
    [SCAppUtils customizeNavigationController:self.navigationController];
        self.title = @"Feedback";
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"cancel"] title:@"" target:self action:@selector(dismiss)] autorelease];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"send"] title:@"" target:self action:@selector(send)] autorelease];
    
    self.container_view.contentSize = self.content_view.frame.size;
    [self.container_view addSubview:self.content_view];
    [self registerForKeyboardNotifications];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.name = nil;
    self.email = nil;
    self.issues = nil;
    self.suggestion = nil;
    self.content_view = nil;
    self.container_view = nil;
    [self removeObserver:self forKeyPath:UIKeyboardDidShowNotification];
    [self removeObserver:self forKeyPath:UIKeyboardWillHideNotification];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)dismiss{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}


-(IBAction)send{

    NSMutableDictionary *feedback = [[[NSMutableDictionary alloc] init] autorelease];
    [feedback setObject:name.text forKey:@"name"];
    [feedback setObject:email.text forKey:@"email"];
    [feedback setObject:suggestion.text forKey:@"suggestion"];
    [feedback setObject:[issues titleForSegmentAtIndex:issues.selectedSegmentIndex] forKey:@"issue"];
    if (self.club) {
        [feedback setObject:club.club_id forKey:@"club_id"];
    }
    if(self.schedule){
        [feedback setObject:schedule.schedule_id forKey:@"schedule_id"];
    }
    
    [self.serviceManager postFeedback:feedback sender:self];
}

#pragma mark - ASIHTTPRequestDelegate

-(void)requestFinished:(ASIHTTPRequest *)request{
    // show confirmation
    request.delegate = nil;
    [self dismiss];
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [FlurryAnalytics logEvent:@"feedback request failed" withParameters:[NSDictionary dictionaryWithObject:request forKey:@"request"]];
    // do something else
    request.delegate = nil;
    [self dismiss];
}

#pragma mark - keyboard related
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    float insetHeight = kbSize.height - (self.view.frame.size.height - container_view.frame.origin.y - container_view.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, insetHeight, 0.0);
    self.container_view.contentInset = contentInsets;
    self.container_view.scrollIndicatorInsets = contentInsets;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.container_view.contentInset = contentInsets;
    self.container_view.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGPoint scrollPoint = CGPointMake(0.0f, textField.frame.origin.y - 40);
    [self.container_view setContentOffset:scrollPoint animated:YES];
    [suggestion setSelectedRange:NSMakeRange(0, 0)];
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0.0f, textView.frame.origin.y);
    [self.container_view setContentOffset:scrollPoint animated:YES];
    [suggestion setSelectedRange:NSMakeRange(0, 0)];
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:self.name]){
        [name resignFirstResponder];
        [email becomeFirstResponder];
    }
    else if([textField isEqual:self.email]){
        [suggestion becomeFirstResponder];
        [email resignFirstResponder];
    }
    return YES;
}
@end
