//
//  goodlifeViewController.m
//  goodlife
//
//  Created by Brian Ge on 11-10-26.
//  Copyright 2011 T4G. All rights reserved.
//

#import "goodlifeViewController.h"
#import "ClubsViewController.h"
@interface goodlifeViewController () 
@property (nonatomic, retain) NSIndexPath *selected_index_path;
@property (nonatomic, retain) NSIndexPath *control_row_index_path;
@end

@implementation goodlifeViewController


@synthesize club_button, class_button, reset_location_button, map_view_button, list_view_button, filter_button, container_view, clubs_map_view, clubs_list_view, group_classes_list_view, club_cell, club_options_cell;
@synthesize clubs, schedules, selected_schedule, currentLocation;
@synthesize serviceManager;

// private properties
@synthesize selected_index_path, control_row_index_path;

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.club_button = nil;
    self.class_button = nil;
    self.reset_location_button = nil;
    self.map_view_button = nil;
    self.list_view_button = nil;
    self.filter_button = nil;
    self.container_view = nil;
    self.clubs_map_view = nil;
    self.clubs_list_view = nil;
    self.group_classes_list_view = nil;
    self.club_cell = nil;
    self.club_options_cell = nil;
}

- (void)dealloc
{
    [self.clubs_map_view removeFromSuperview]; // release crashes app
    //[self.clubs release];
    //[self.currentLocation release];
    //[self.serviceManager release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.container_view];
    
    serviceManager = [ServiceManager sharedManager];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
- (NSIndexPath *)modelIndexPathforIndexPath:(NSIndexPath *)indexPath
{
    if(self.control_row_index_path != nil){
        int whereIsTheControlRow = self.control_row_index_path.row;
        if(indexPath.row >= whereIsTheControlRow){
            return [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:0]; 
        }
    }
    return indexPath;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(tableView == self.clubs_list_view){
        return 1; // only 1 section should be enough
    }
    if(tableView == self.group_classes_list_view){
        return 7; // 7 days in a week
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if(tableView == self.clubs_list_view){
        if(self.control_row_index_path){
            return self.clubs.count+1;
        }
        else{
            return self.clubs.count;
        }
    }
    else if(tableView == self.group_classes_list_view){
        NSArray *classes_of_day = [self.selected_schedule.group_classes objectForKey:[NSNumber numberWithInt: section]];
        return classes_of_day.count;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(tableView == self.group_classes_list_view){
        switch (section) {
            case 0:
                return @"Monday";
                break;
            case 1:
                return @"Tuesday";
                break;
            case 2:
                return @"Wednesday";
                break;
            case 3:
                return @"Thursday";
                break;
            case 4:
                return @"Friday";
                break;
            case 5:
                return @"Saturday";
                break;
            case 6:
                return @"Sunday";
                break;
                
            default:
                return @"";
                break;
        }
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.clubs_list_view){

        if([self.control_row_index_path isEqual:indexPath]){
        
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"club_options_cell"];
            
            
            if(cell == nil){
                [[NSBundle mainBundle] loadNibNamed:@"clubOptionsCell" owner:self options:nil];
                cell = club_options_cell;
                self.club_options_cell = nil;
            }
            
            UIButton *btn;
            btn = (UIButton *)[cell viewWithTag:9];
//            [btn setTitle:club.subtitle forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(ClubOptionsNavigateBtnTapped:) forControlEvents:UIControlEventTouchUpInside];

            btn = (UIButton *)[cell viewWithTag:1];
//            [btn setTitle:((Schedule *)[club.schedules objectAtIndex:0]).name forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(ClubOptionsViewSchedulesBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"club_cell"];
            if (cell == nil) {

                [[NSBundle mainBundle] loadNibNamed:@"clubCell" owner:self options:nil];
                cell = club_cell;
                self.club_cell = nil;
            }
            Club *club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath:indexPath].row];
            
            // Configure the cell...
            
            UILabel *label;
            label = (UILabel *)[cell viewWithTag:1];
            label.text = club.title;
            
            label = (UILabel *)[cell viewWithTag:2];
            label.text = club.subtitle;
        
            return cell;
        }
    }
    if(tableView == self.group_classes_list_view){
        static NSString *CellIdentifier = @"group_class_cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        }
        
        GroupClass *group_class = [(NSArray *)[self.selected_schedule.group_classes objectForKey:[NSNumber numberWithInt:indexPath.section]] objectAtIndex:indexPath.row];
        // Configure the cell...
        cell.textLabel.text = group_class.name;
        NSDateFormatter *timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [timeFormatter setDateFormat:@"hh:mm a"];
        NSString *start = [timeFormatter stringFromDate:group_class.start_time];
        NSString *end = [timeFormatter stringFromDate:group_class.end_time];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", start, end];
        return cell;
    }
    else {
        NSLog(@"something is wrong... hmmm...");
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.clubs_list_view){
        if([indexPath isEqual:self.control_row_index_path]){
            return 30; //height for control cell
        }
    }
    return 44;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.clubs_list_view){
        //if user tapped the same row twice let's start getting rid of the control cell
        if([indexPath isEqual:self.selected_index_path]){
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
        
        //update the indexpath if needed... I explain this below 
        indexPath =  [self modelIndexPathforIndexPath:indexPath];
        
        //pointer to delete the control cell
        NSIndexPath *indexPathToDelete = [NSIndexPath indexPathForRow:self.control_row_index_path.row inSection:self.control_row_index_path.section];

        //if in fact I tapped the same row twice lets clear our tapping trackers 
        if([indexPath isEqual:self.selected_index_path]){
            self.selected_index_path = nil;
            self.control_row_index_path = nil;        
        }
        //otherwise let's update them appropriately 
        else{
            self.selected_index_path = indexPath; //the row the user just tapped. 
            //Now I set the location of where I need to add the dummy cell 
            self.control_row_index_path = [NSIndexPath indexPathForRow:indexPath.row + 1   inSection:indexPath.section];
        }
        
        //all logic is done, lets start updating the table
        [tableView beginUpdates];
        
        //lets delete the control cell, either the user tapped the same row twice or tapped another row
        if(indexPathToDelete.row != 0){
            NSLog(@"row to delete %d", indexPathToDelete.row);
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
        }
        //lets add the new control cell in the right place 
        if(self.control_row_index_path){
            
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:self.control_row_index_path] 
                                  withRowAnimation:UITableViewRowAnimationNone];
        }
        
        //and we are done... 
        [tableView endUpdates];
        //[tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];

    }
}

- (void) ClubOptionsNavigateBtnTapped:(id)sender{
    UIView *contentView = [sender superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [self.clubs_list_view indexPathForCell:cell];
    Club* club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath: indexPath].row];
    
    NSString* url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%f,%f&daddr=%@",
                     self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude,
                     [club.subtitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}

- (void) ClubOptionsViewSchedulesBtnTapped:(id) sender{
    
    UIView *contentView = [sender superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [self.clubs_list_view indexPathForCell:cell];
    Club* club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath: indexPath].row];    
    [self.serviceManager getSchedules:club.club_id.intValue sender:self];
}

#pragma mark - MapViewDelegates

-(void)mapViewWillStartLocatingUser:(MKMapView *)mapView{
    [mapView removeAnnotations:mapView.annotations];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    self.currentLocation = mapView.userLocation;    // set currentLocation
    [self zoomMapViewToUserLocation];    // zoom to user location
    mapView.showsUserLocation = NO;    // stop locating user
}

-(void) mapViewDidStopLocatingUser:(MKMapView *)mapView{
    [mapView addAnnotation: self.currentLocation];    // add annotation to user location
    [serviceManager getClubsByLocation:self.currentLocation.coordinate within:10 sender:self];    // request for clubs
}


- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    NSLog(@"map view region changed");
//    // maybe we can implement something here to refresh clubs in a not so annoying way
//    
//    //To calculate the search bounds...
//    //First we need to calculate the corners of the map so we get the points
//    CGPoint nePoint = CGPointMake(mapView.bounds.origin.x + mapView.bounds.size.width, mapView.bounds.origin.y);
//    CGPoint swPoint = CGPointMake((mapView.bounds.origin.x), (mapView.bounds.origin.y + mapView.bounds.size.height));
//    
//    //Then transform those point into lat,lng values
//    CLLocationCoordinate2D neCoord = [mapView convertPoint:nePoint toCoordinateFromView:mapView];
//    CLLocationCoordinate2D swCoord = [mapView convertPoint:swPoint toCoordinateFromView:mapView];
//    
//    [serviceManager getClubsByNECoordinate:neCoord andSWCoordinate:swCoord sender:self];
    
}

-(void) mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views{
    double delay = 0.0;
    CGRect visibleRect = [mapView annotationVisibleRect];
    for (MKAnnotationView *view in views) {
        CGRect endFrame = view.frame;
        CGRect startFrame = endFrame;
        startFrame.origin.y = visibleRect.origin.y - startFrame.size.height;
        view.frame = startFrame;
        [UIView beginAnimations:@"drop" context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelay:delay];
        view.frame = endFrame;
        [UIView commitAnimations];
        delay += 0.1;
    }
}


- (MKAnnotationView *) mapView:(MKMapView *) mapView viewForAnnotation:(id ) annotation {
    if([annotation isKindOfClass:[Club class]]) {
//        NSString *club_name = ((Club*)annotation).name;
//        MKAnnotationView *customAnnotationView = (MKAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: club_name];
//        if(customAnnotationView == nil){
//            customAnnotationView=[[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:club_name] autorelease];
//        }
//        else{
//            customAnnotationView.annotation = annotation;
//        }
//        UIImage *pinImage = [UIImage imageNamed:@"red_pin.png"];
//        [customAnnotationView setImage:pinImage];
//        customAnnotationView.canShowCallout = YES;
//        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        [rightButton addTarget:self action:@selector(annotationViewClick:) forControlEvents:UIControlEventTouchUpInside];
//        customAnnotationView.rightCalloutAccessoryView = rightButton;
//        return customAnnotationView;
//    }
//    else if ([annotation isKindOfClass:[MKPointAnnotation class]]){
        
        NSString *club_name = ((Club*)annotation).name;
        MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: club_name];
        if (pin == nil) {
            pin = [[[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: club_name] autorelease];
        } else {
            pin.annotation = annotation;
        }
        pin.pinColor = MKPinAnnotationColorRed;
        pin.animatesDrop = YES;
        pin.canShowCallout = YES;
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        pin.rightCalloutAccessoryView = rightButton;
        return pin;
    }
    else {
        MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: @"user_location"];
        if (pin == nil) {
            pin = [[[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"user_location"] autorelease];
        } else {
            pin.annotation = annotation;
        }
        pin.pinColor = MKPinAnnotationColorGreen;
        pin.animatesDrop = YES;
        pin.canShowCallout = YES;
        return pin;
    }
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    if ([view.annotation isKindOfClass:[Club class]]){
        NSLog(@"club %@ annotation clicked", ((Club*)view.annotation).club_id );
        [serviceManager getSchedules:((Club*)view.annotation).club_id.intValue sender:self];
    }
}

-(void) zoomMapViewToUserLocation{
    if ([self.clubs_map_view showsUserLocation]) {  
        MKCoordinateRegion region;
        region.center = self.clubs_map_view.userLocation.coordinate;  
        
        MKCoordinateSpan span; 
        span.latitudeDelta  = 0.1; // Change these values to change the zoom
        span.longitudeDelta = 0.1; 
        region.span = span;
        
        [self.clubs_map_view setRegion:region animated:YES];
        // and of course you can use here old and new location values
    }
}

#pragma mark - IBActions

-(IBAction)onClub{
    [self showClubSection];
}
-(IBAction)onClass{
    [self showClassSection];
}

-(IBAction)refreshUserLocation{
    self.clubs_map_view.showsUserLocation = YES;
}
-(IBAction)showMapView{
    self.clubs_list_view.hidden=YES;
    self.clubs_map_view.hidden=NO;
}
-(IBAction)showListView{
    self.clubs_list_view.hidden=NO;
    self.clubs_map_view.hidden=YES;
}
-(IBAction)showFilters{
    ClubsViewController *controller = [[[ClubsViewController alloc] init] autorelease];
    UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
    [self presentModalViewController:nav animated:YES];
}

-(void)showClubSection{
    CGRect theFrame = [self.container_view frame];
    theFrame.origin.y = 0.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [self.container_view setFrame:theFrame];
    [UIView commitAnimations];
}
-(void)showClassSection{
    CGRect theFrame = [self.container_view frame];
    theFrame.origin.y = -395.f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [self.container_view setFrame:theFrame];
    [UIView commitAnimations];
}

#pragma mark - ASIHTTPRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"%@", request.responseString);
    NSLog(@"%@",[request.userInfo objectForKey:@"request_id"]);
    if ([[request.userInfo objectForKey:@"request_id"] hasPrefix:@"getClubs"]) {
        // parse json to clubs
        self.clubs = [ModelObjectFactory ClubsFromJSONString:request.responseString];
        // add annotations 
        for( Club *club in self.clubs){
            [self.clubs_map_view addAnnotation:club];   
        }
        // refresh clubs table view
        [self.clubs_list_view reloadData];
    }
    else if([[request.userInfo objectForKey:@"request_id"] isEqualToString:@"getSchedules"]){
        self.schedules = [ModelObjectFactory SchedulesFromJSONString:request.responseString];
        if(self.schedules.count > 0){
            self.selected_schedule = [self.schedules objectAtIndex:0];
        }
        // refresh schedules list view
        [self.group_classes_list_view reloadData];
        [self showClassSection];
    }
}


- (void)requestFailed:(ASIHTTPRequest *)request
{

    NSLog(@"%@", request.error.description);

}

@end
