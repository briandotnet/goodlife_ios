//
//  ScheduleDetailsViewController.m
//  goodlife
//
//  Created by Brian Ge on 12-02-02.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "ScheduleDetailsViewController.h"
#import <EventKit/EventKit.h>

@implementation ScheduleDetailsViewController

@synthesize club_name_label, club_map_view, schedule_name_label, effective_dates_label, classes_table_view, class_cell;
@synthesize schedule, club, selected_calss, serviceManager;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    [TestFlight passCheckpoint:@"memory warning at schedule details view"];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.serviceManager = [ServiceManager sharedManager];
    UIImage *backImage = [UIImage imageNamed:@"back.png"];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithImage:backImage title:@"" target:self action:@selector(back)] autorelease];
    self.title = @"Schedule";
    MKCoordinateRegion region;
    region.center = CLLocationCoordinate2DMake(self.club.latitude, self.club.longitude);  
    
    MKCoordinateSpan span; 
    span.latitudeDelta  = 0.01; // Change these values to change the zoom
    span.longitudeDelta = 0.01; 
    region.span = span;
    
    [self.club_map_view setRegion:region animated:NO];
    [self.club_map_view addAnnotation:self.club];
    
    self.club_name_label.text = club.name;
    [self.club_name_label setAdjustsFontSizeToFitWidth:YES];

    self.schedule_name_label.text = self.schedule.name;
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    self.effective_dates_label.text = [NSString stringWithFormat:@"%@ - %@",
                                       [dateFormatter stringFromDate:schedule.start_date],
                                       [dateFormatter stringFromDate:schedule.end_date]];
    [self.serviceManager getSchedule:self.schedule.schedule_id.intValue sender:self];
    [self showHud];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.club_name_label = nil;
    self.club_map_view = nil;
    self.schedule_name_label = nil;
    self.effective_dates_label = nil;
    self.classes_table_view = nil;
    self.class_cell = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Map 
- (MKAnnotationView *) mapView:(MKMapView *) mapView viewForAnnotation:(id ) annotation {
    MKPinAnnotationView *pin;
    if([annotation isKindOfClass:[Club class]]) {
        
        NSString *club_name = ((Club*)annotation).name;
        pin = [[[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: club_name] autorelease];
        pin.pinColor = MKPinAnnotationColorRed;
        pin.animatesDrop = NO;
        pin.canShowCallout = NO;
    }
    else{
        pin = [[[MKPinAnnotationView alloc] init] autorelease];
        pin.pinColor = MKPinAnnotationColorPurple;
    }
    return pin;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.schedule.group_classes.allKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *classes_of_day = [self.schedule.group_classes objectForKey:[NSNumber numberWithInt: section]];
    return classes_of_day.count;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    // calculate header date
    // sunday 1 saturday 7
    int class_weekday = section == 6 ? 1 : section + 2;
    int today_weekday = [[[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:[NSDate date]] weekday];    
    int day_diff = class_weekday - today_weekday;
    NSDate *header_date = [[NSDate date] dateByAddingTimeInterval:day_diff*24*60*60];
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];

            
    UIView *header_view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 288, 30)] autorelease];
    
    header_view.backgroundColor = [UIColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:0.8f];
    
    // set labels
    [dateFormatter setDateFormat:@"EEEE"];
    UILabel *week_day_label = [[[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)] autorelease];
    week_day_label.text = [dateFormatter stringFromDate:header_date];
    week_day_label.textAlignment = UITextAlignmentLeft;
    week_day_label.backgroundColor = [UIColor clearColor];
    week_day_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    week_day_label.textColor = [UIColor whiteColor];
    week_day_label.shadowColor = [UIColor lightGrayColor];
    week_day_label.shadowOffset = CGSizeMake(0, 1);
    
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    UILabel *date_label = [[[UILabel alloc] initWithFrame:CGRectMake(110, 0, 158, 30)] autorelease];
    date_label.text = [dateFormatter stringFromDate:header_date];
    date_label.textAlignment = UITextAlignmentRight;
    date_label.backgroundColor = [UIColor clearColor];
    date_label.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    date_label.textColor = [UIColor whiteColor];
    date_label.shadowColor = [UIColor lightGrayColor];
    date_label.shadowOffset = CGSizeMake(0, 1);
    
    [header_view addSubview:date_label];
    [header_view addSubview:week_day_label];
    if([[dateFormatter stringFromDate:[NSDate date]] isEqualToString:date_label.text]){
        header_view.backgroundColor = [UIColor colorWithRed:0.8f green:0.2f blue:0.2f alpha:0.8f];
    }

    return header_view;
        
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
        return 30;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"class_cell"];
    if (cell == nil) {
        
        [[NSBundle mainBundle] loadNibNamed:@"classCell" owner:self options:nil];
        cell = class_cell;
        self.class_cell = nil;
    }
    GroupClass *group_class = [(NSArray *)[self.schedule.group_classes objectForKey:[NSNumber numberWithInt:indexPath.section]] objectAtIndex:indexPath.row];
            
    UILabel *label;
    UIImageView *imageView;
    UIButton *button;
    NSDateFormatter *timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [timeFormatter setDateFormat:@"h:mm a"];
    [timeFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
    imageView = (UIImageView *)[cell viewWithTag:2];    
    UIImage *image = [UIImage imageNamed:[group_class.icon lowercaseString]];
    if(!image){
        image = [UIImage imageNamed:@"icon_boarder.png"];
        image = [image stretchableImageWithLeftCapWidth:15 topCapHeight:15];
    }
    imageView.image = image;
    
    label = (UILabel *)[cell viewWithTag:1];
    label.text = group_class.name;
    label = (UILabel *)[cell viewWithTag:3];
    label.text = [[timeFormatter stringFromDate:group_class.start_time] lowercaseString];
    label = (UILabel *)[cell viewWithTag:4];
    label.text = [[timeFormatter stringFromDate:group_class.end_time] lowercaseString];
    button = (UIButton *)[cell viewWithTag:5];
    [button addTarget:self action:@selector(addClassToCalendar:event:) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundView= [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"club_cell_bg.png"]] autorelease];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 66;
}

#pragma mark - Table view delegate

- (void) addClassToCalendar:(id)sender event:(id)event{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.classes_table_view];
    NSIndexPath *indexPath = [self.classes_table_view indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        self.selected_calss = [(NSArray *)[self.schedule.group_classes objectForKey:[NSNumber numberWithInt:indexPath.section]] objectAtIndex:indexPath.row];
        
        NSString *title = [NSString stringWithFormat: @"Add %@ to your calendar?", self.selected_calss.name];
        NSString *message = nil;
        NSString *once_button_title = [NSString stringWithFormat:@"Once on %@", self.selected_calss.day_of_week_desc];
        NSString *weekly_button_tittle = [NSString stringWithFormat:@"Every %@", self.selected_calss.day_of_week_desc];

        if ([self.selected_calss.start_time timeIntervalSinceNow] < 0.0f) {
            // class is in the past. we need to add 7 days to class.start_time
            message = @"The class you picked is in the past. We picked the same class next week for you.";
            once_button_title = [NSString stringWithFormat:@"Once next %@", self.selected_calss.day_of_week_desc];
        }
        
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:once_button_title , weekly_button_tittle, nil] autorelease];
        alert.tag = 1;
        [alert show];
    }
}

#pragma mark - ASIHTTPRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    @try {
        // parse json to clubs
        self.schedule = [ModelObjectFactory ScheduleFromJSONString:request.responseString];
        [UIView beginAnimations:@"hide_clubs_list" context:NULL];
        [UIView setAnimationDuration:1];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.classes_table_view cache:NO];
        [UIView commitAnimations];
        [self.classes_table_view reloadData];
        // scroll to today's section
        // sunday 1 saturday 7
        int today_weekday = [[[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:[NSDate date]] weekday];
        int section = today_weekday == 1 ? 6 : today_weekday - 2;
        [self.classes_table_view scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        

    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Oops..." message:@"Looks like a mouse chewed up some wire on our server. We are trying to fix it now. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] autorelease];
        alert.tag = 1;
        [alert show];

    }
    @finally {
        [request.delegate release];
        [HUD hide:YES];
    }
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    [HUD hide:YES];
    [FlurryAnalytics logError:@"request_error" message:request.url.absoluteString error:request.error];
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Oops..." message:@"We lost what you were looking for on the way. Would you like me to make another trip?" delegate:self cancelButtonTitle:@"Nah I'm cool" otherButtonTitles:@"Try again now!", nil] autorelease];
    alert.tag = 0;
    [alert show];
    [request.delegate release];
}

#pragma mark - UIAlertViewDelegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    EKCalendar *destination_calendar;
    switch (alertView.tag) {
        case 0: // network error
            if (buttonIndex == 1) { // try load the schedule again
                [self.serviceManager getSchedule:self.schedule.schedule_id.intValue sender:self];
                [self showHud];
            }
            break;
        case 1: // add to calendar? No / Once / Weekly
            switch (buttonIndex) {
                case 0: // do nothing
                    break;
                case 1: // add once
                    destination_calendar = [self getCalendar];
                    [self createEventForClass:self.selected_calss inCalendar:destination_calendar repeatWeekly:NO];
                    break;
                case 2: // add weekly event
                    destination_calendar = [self getCalendar];
                    [self createEventForClass:self.selected_calss inCalendar:destination_calendar repeatWeekly:YES];
                    break;
                default:
                    break;
            }
            break;
        case 2: // error from request schedule
            
            break;
        default:
            break;
    }
}

#pragma mark MBProgressHUDDelegate methods

- (void)showHud{
    HUD = [[[MBProgressHUD alloc] initWithView:self.navigationController.view] autorelease];
    HUD.delegate = self;
    HUD.labelText = @"Loading Schedule";
    [HUD show:YES];
	[self.navigationController.view addSubview:HUD];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
	HUD = nil;
}


-(IBAction) viewInBrowser{
    WebViewController *webview = [[[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil] autorelease];
    webview.url = [NSURL URLWithString:schedule.url];
    webview.title = schedule.name;    
    UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:webview] autorelease];
    [SCAppUtils customizeNavigationController:nav];
    [self.navigationController presentModalViewController:nav animated:YES];

}

-(EKCalendar *) getCalendar{
    EKEventStore *eventDB = [[[EKEventStore alloc] init] autorelease];
    EKCalendar *destinationCalendar = nil;;
    NSString *calendar_name = @"My GoodLife Calendar";
    // first try to see if we already have this calendar 
    for (EKCalendar *cal in eventDB.calendars) {
        if ([cal.title isEqualToString:calendar_name]) {
            destinationCalendar = cal;
            break;
        }
    }
    
    if (!destinationCalendar) { // nope we don't have this calendar, try to create one
        
        if ([eventDB respondsToSelector:@selector(saveCalendar:commit:error:)]) {
            // iOS 5+
            EKSource *localSource = nil;
            for (EKSource *source in eventDB.sources) {
                if(source.sourceType == EKSourceTypeLocal){
                    localSource = source; // find local source
                    break;
                }
            }
            if (localSource) { // if we have a local source, we can create a local calendar
                destinationCalendar = [EKCalendar calendarWithEventStore:eventDB];
                destinationCalendar.source = localSource;
                destinationCalendar.title = calendar_name;
                NSError *calendar_save_err;
                [eventDB saveCalendar:destinationCalendar commit:YES error:&calendar_save_err];
                if (calendar_save_err == noErr) {
                    [FlurryAnalytics logEvent:@"My GoodLife Calendar created"];
                }
                else{
                    [FlurryAnalytics logError:@"error_create_calendar" message:calendar_save_err.description error:calendar_save_err];
                }
            }
        }
    }
    return destinationCalendar ? destinationCalendar : [eventDB defaultCalendarForNewEvents];
}

-(void) createEventForClass: (GroupClass *)class inCalendar:(EKCalendar *)calendar repeatWeekly:(BOOL)repeatWeekly{

    BOOL next_week = NO;
    if ([class.start_time timeIntervalSinceNow] < 0.0f) {
        // class is in the past. we need to add 7 days to class.start_time
        class.start_time = [NSDate dateWithTimeInterval:7*24*60*60 sinceDate:class.start_time];
        next_week = YES;
    }
    
    EKEventStore *eventDB = [[[EKEventStore alloc] init] autorelease];
    
    EKEvent *myEvent  = [EKEvent eventWithEventStore:eventDB];
    myEvent.title     = class.name;
    myEvent.startDate = class.start_time;
    myEvent.endDate   = [class.start_time dateByAddingTimeInterval:class.duration.intValue*60];
    myEvent.location = [NSString stringWithFormat:@"%@, %@", self.club.title, self.club.subtitle];
    myEvent.alarms = [NSArray arrayWithObject:[EKAlarm alarmWithRelativeOffset:-15*60]];
    if(repeatWeekly){
        EKRecurrenceRule* rule = [[[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly interval:1 end:[EKRecurrenceEnd recurrenceEndWithEndDate:schedule.end_date]] autorelease];
        if ([myEvent respondsToSelector:@selector(setRecurrenceRules:)]){
            myEvent.recurrenceRules = [NSArray arrayWithObject: rule]; // iOS 5+
        }
        else{
            myEvent.recurrenceRule = rule; // below iOS 5
        }
    }
    
    [myEvent setCalendar:calendar];
    NSError *err;
    [eventDB saveEvent:myEvent span:EKSpanThisEvent error:&err];
    if (err == noErr) {
        NSString *message_time;
        if (repeatWeekly) {
            message_time = [NSString stringWithFormat:@" every %@",class.day_of_week_desc];
        }
        else{
            
            NSDateFormatter *timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
            [timeFormatter setDateFormat:@"hh:mm a"];
            NSLocale *locale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
            [timeFormatter setLocale:locale];
            
            
            message_time = next_week ?
                            [NSString stringWithFormat:@" on %@ at %@", class.day_of_week_desc, [timeFormatter stringFromDate:class.start_time]] :
                            [NSString stringWithFormat:@" next %@ at %@", class.day_of_week_desc, [timeFormatter stringFromDate:class.start_time]]
                            ;
        }
        NSString *message = [NSString stringWithFormat:@"%@ has been added to your calendar%@!", class.name, message_time];

        UIAlertView *alert = [[[UIAlertView alloc]
                               initWithTitle:@""
                               message:message
                               delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] autorelease];
        [alert show];
    }
    else{
        [FlurryAnalytics logError:@"error_save_event" message:err.description error:err];
        UIAlertView *alert = [[[UIAlertView alloc]
                               initWithTitle:@"Something went wrong :( Please let us know."
                               message:err.description
                               delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] autorelease];
        [alert show];
        
    }
    [FlurryAnalytics logEvent:@"Add Class to calendar" withParameters:[NSDictionary dictionaryWithObject:class forKey:@"group_class"]];
}
@end
