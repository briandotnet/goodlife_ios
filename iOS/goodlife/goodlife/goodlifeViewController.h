//
//  goodlifeViewController.h
//  goodlife
//
//  Created by Brian Ge on 11-10-26.
//  Copyright 2011 T4G. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ServiceManager.h"
#import "ASIHTTPRequest.h"
#import "Club.h"
#import "ModelObjectFactory.h"


@interface goodlifeViewController : UIViewController<MKMapViewDelegate, ASIHTTPRequestDelegate, UITableViewDataSource, UITableViewDelegate> {
    UIButton *club_button;
    UIButton *class_button;
    UIButton *reset_location_button;
    UIButton *map_view_button;
    UIButton *list_view_button;
    UIButton *filter_button;
    UIView *container_view;
    MKMapView *clubs_map_view;
    UITableView *clubs_list_view;
    UITableView *group_classes_list_view;
    UITableViewCell *club_cell;
    UITableViewCell *club_options_cell;
    
    NSArray *clubs;
    NSArray *schedules;
    Schedule *selected_schedule;
    
    NSObject<MKAnnotation> *currentLocation;
    
    ServiceManager *serviceManager;
    @private
    NSIndexPath *selected_index_path;
    NSIndexPath *control_row_index_path;
}


@property (nonatomic, retain) IBOutlet UIButton *club_button;
@property (nonatomic, retain) IBOutlet UIButton *class_button;
@property (nonatomic, retain) IBOutlet UIButton *reset_location_button;
@property (nonatomic, retain) IBOutlet UIButton *map_view_button;
@property (nonatomic, retain) IBOutlet UIButton *list_view_button;
@property (nonatomic, retain) IBOutlet UIButton *filter_button;
@property (nonatomic, retain) IBOutlet UIView *container_view;
@property (nonatomic, retain) IBOutlet MKMapView *clubs_map_view;
@property (nonatomic, retain) IBOutlet UITableView *clubs_list_view;
@property (nonatomic, retain) IBOutlet UITableView *group_classes_list_view;
@property (nonatomic, assign) IBOutlet UITableViewCell *club_cell;
@property (nonatomic, assign) IBOutlet UITableViewCell *club_options_cell;

@property (nonatomic, retain) NSArray *clubs;
@property (nonatomic, retain) NSArray *schedules;
@property (nonatomic, retain) Schedule *selected_schedule;

@property (nonatomic, retain) NSObject<MKAnnotation> *currentLocation;

@property (nonatomic, retain) ServiceManager *serviceManager;
-(IBAction)onClub;
-(IBAction)onClass;
-(IBAction)refreshUserLocation;
-(IBAction)showMapView;
-(IBAction)showListView;
-(IBAction)showFilters;


-(void)showClubSection;
-(void)showClassSection;

//map related functions
-(void) zoomMapViewToUserLocation;

@end
