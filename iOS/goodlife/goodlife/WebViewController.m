//
//  WebViewController.m
//  goodlife
//
//  Created by Brian Ge on 12-02-04.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

@synthesize url, webview, loading_indicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [FlurryAnalytics logEvent:@"WebViewController loaded" withParameters:[NSDictionary dictionaryWithObject:self.url forKey:@"URL"]];
    [SCAppUtils customizeNavigationController:self.navigationController];
    UIImage *backImage = [UIImage imageNamed:@"cancel"];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithImage:backImage title:@"" target:self action:@selector(dismiss)] autorelease];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:self.loading_indicator] autorelease];
    
    [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.webview = nil;
    self.loading_indicator = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) webViewDidFinishLoad:(UIWebView *)webView{
    [self.loading_indicator stopAnimating];
}

-(void) webViewDidStartLoad:(UIWebView *)webView{
    [self.loading_indicator startAnimating];
}

-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;//[request.URL isEqual:self.url];
}

-(void)dismiss{
    [self.webview stopLoading];
    self.webview.delegate = nil;
    [self.navigationController dismissModalViewControllerAnimated:YES];
}
@end
