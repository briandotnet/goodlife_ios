//
//  ClubsViewController.h
//  goodlife
//
//  Created by Brian Ge on 12-01-27.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <EventKit/EventKit.h>
#import "ServiceManager.h"
#import "ASIHTTPRequest.h"
#import "Club.h"
#import "ModelObjectFactory.h"
#import "UIBarButtonItem.h"
#import "ClubDetailsViewController.h"
#import "ScheduleDetailsViewController.h"
#import "CalloutMapAnnotation.h"
#import "BasicMapAnnotationView.h"
#import "CalloutMapAnnotationView.h"
#import "AccessorizedCalloutMapAnnotationView.h"
#import "BlockActionSheet.h"
#import "TestFlight.h"
#import "MBProgressHUD.h"

@interface ClubsViewController : UIViewController<MKMapViewDelegate, ASIHTTPRequestDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, MBProgressHUDDelegate>{
    
    //UIButton *reset_location_button;
    UIButton *map_view_button;
    UIButton *list_view_button;
    //UIButton *search_button;
    UIView *search_view;
    UIButton *search_cancel_button;
    UITextField *search_text_field;
    UIImageView *pointer;
    MKMapView *clubs_map_view;
	MKAnnotationView *_selectedAnnotationView;
    
    UITableView *clubs_list_view;
    
    UITableViewCell *club_cell;
    UITableViewCell *club_options_cell;
    
    MBProgressHUD *HUD;
    
    NSArray *clubs;
    NSObject<MKAnnotation> *currentLocation;
    ServiceManager *serviceManager;

@private
	CalloutMapAnnotation *_calloutAnnotation;
    
    NSIndexPath *selected_index_path;
    NSIndexPath *control_row_index_path;
}

//@property (nonatomic, retain) IBOutlet UIButton *reset_location_button;
@property (nonatomic, retain) IBOutlet UIButton *map_view_button;
@property (nonatomic, retain) IBOutlet UIButton *list_view_button;
//@property (nonatomic, retain) IBOutlet UIButton *search_button;
@property (nonatomic, retain) IBOutlet UIView *search_view;
@property (nonatomic, retain) IBOutlet UIButton *search_cancel_button;
@property (nonatomic, retain) IBOutlet UITextField *search_text_field;
@property (nonatomic, retain) IBOutlet UIImageView *pointer;
@property (nonatomic, retain) IBOutlet MKMapView *clubs_map_view;
@property (nonatomic, retain) MKAnnotationView *selectedAnnotationView;
@property (nonatomic, retain) IBOutlet UITableView *clubs_list_view;
@property (nonatomic, assign) IBOutlet UITableViewCell *club_cell;
@property (nonatomic, assign) IBOutlet UITableViewCell *club_options_cell;

@property (nonatomic, retain) NSArray *clubs;
@property (nonatomic, retain) NSObject<MKAnnotation> *currentLocation;
@property (nonatomic, retain) ServiceManager *serviceManager;

-(IBAction)locateNearByClubs;
-(IBAction)showSearchView;
-(IBAction)hideSearchView;
-(IBAction)showMapView;
-(IBAction)showListView;

- (void)showHud;
-(void) zoomMapViewToCoordinate:(CLLocationCoordinate2D)coordinate;
@end
