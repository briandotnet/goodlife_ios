//
//  goodlifeAppDelegate.h
//  goodlife
//
//  Created by Brian Ge on 11-10-26.
//  Copyright 2011 T4G. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface goodlifeAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

//@property (nonatomic, retain) IBOutlet ClubsViewController *viewController;

@end
