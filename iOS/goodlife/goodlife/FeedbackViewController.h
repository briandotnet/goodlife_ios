//
//  FeedbackViewController.h
//  goodlife
//
//  Created by Brian Ge on 12-02-05.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Club.h"
#import "Schedule.h"
#import "ServiceManager.h"
#import "SCAppUtils.h"
#import "ASIHTTPRequest.h"
#import "UIBarButtonItem.h"

@interface FeedbackViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, ASIHTTPRequestDelegate>{
    Club *club;
    Schedule *schedule;
    ServiceManager *serviceManager;
    
    UISegmentedControl *issues;
    UITextView *suggestion;
    UITextField *name;
    UITextField *email;
    UIScrollView *container_view;
    UIView *content_view;
    
}
@property(nonatomic, retain) Club *club;
@property(nonatomic, retain) Schedule *schedule;
@property(nonatomic, retain) ServiceManager *serviceManager;
@property(nonatomic, retain) IBOutlet UISegmentedControl *issues;
@property(nonatomic, retain) IBOutlet UITextView *suggestion;
@property(nonatomic, retain) IBOutlet UITextField *name;
@property(nonatomic, retain) IBOutlet UITextField *email;
@property(nonatomic, retain) IBOutlet UIScrollView *container_view;
@property(nonatomic, retain) IBOutlet UIView *content_view;

- (void)registerForKeyboardNotifications;
-(IBAction)dismiss;
-(IBAction)send;

@end
