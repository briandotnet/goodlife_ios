//
//  ClubsViewController.m
//  goodlife
//
//  Created by Brian Ge on 12-01-27.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "ClubsViewController.h"

@interface ClubsViewController () 

@property (nonatomic, retain) CalloutMapAnnotation *calloutAnnotation;
@property (nonatomic, retain) NSIndexPath *selected_index_path;
@property (nonatomic, retain) NSIndexPath *control_row_index_path;

-(void) getClubDirections:(Club *)club;
-(void) getClubPhoneNumber:(Club *)club;
-(void) getClubSchedules:(Club *)club;
-(void) getClubDetails:(Club *)club;
@end

@implementation ClubsViewController

@synthesize map_view_button, list_view_button, search_view, search_text_field, search_cancel_button, pointer, clubs_map_view, clubs_list_view, club_cell, club_options_cell;
@synthesize clubs, currentLocation, serviceManager;

@synthesize calloutAnnotation = _calloutAnnotation;
@synthesize selectedAnnotationView = _selectedAnnotationView;

// private properties
@synthesize selected_index_path, control_row_index_path;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    [TestFlight passCheckpoint:@"memory warning at clubs view"];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    serviceManager = [ServiceManager sharedManager];
    
    if ([[UINavigationBar class]respondsToSelector:@selector(appearance)]) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_blank.png"] forBarMetrics:UIBarMetricsDefault];
    }
    self.title = @"Goodlife Clubs";

    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"locate.png"] title:@"" target:self action:@selector(locateNearByClubs)] autorelease];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"find.png"] title:@"" target:self action:@selector(showSearchView)] autorelease];
    [self showHud];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //self.reset_location_button = nil;
    self.map_view_button = nil;
    self.list_view_button = nil;
    //self.search_button = nil;
    self.pointer = nil;
    self.clubs_map_view = nil;
    self.clubs_list_view = nil;
    self.club_cell = nil;
    self.club_options_cell = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
- (NSIndexPath *)modelIndexPathforIndexPath:(NSIndexPath *)indexPath
{
    if(self.control_row_index_path != nil){
        int whereIsTheControlRow = self.control_row_index_path.row;
        if(indexPath.row >= whereIsTheControlRow){
            return [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:0]; 
        }
    }
    return indexPath;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.clubs_list_view.hidden = (self.clubs.count == 0);
    // Return the number of rows in the section.
    if(self.control_row_index_path){
        return self.clubs.count+1;
    }
    else{
        return self.clubs.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    if([self.control_row_index_path isEqual:indexPath]){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"club_options_cell"];
        
        
        if(cell == nil){
            [[NSBundle mainBundle] loadNibNamed:@"clubOptionsCell" owner:self options:nil];
            cell = club_options_cell;
            self.club_options_cell = nil;
        }
        
        UIButton *btn;
        btn = (UIButton *)[cell viewWithTag:1];
        //            [btn setTitle:club.subtitle forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(ClubOptionsNavigateBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        btn = (UIButton *)[cell viewWithTag:2];
        //            [btn setTitle:((Schedule *)[club.schedules objectAtIndex:0]).name forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(ClubOptionsCallBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        btn = (UIButton *)[cell viewWithTag:3];
        //            [btn setTitle:((Schedule *)[club.schedules objectAtIndex:0]).name forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(ClubOptionsSchedulesBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        btn = (UIButton *)[cell viewWithTag:4];
        //            [btn setTitle:((Schedule *)[club.schedules objectAtIndex:0]).name forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(ClubOptionsDetailsBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
    }
    else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"club_cell"];
        if (cell == nil) {
            
            [[NSBundle mainBundle] loadNibNamed:@"clubCell" owner:self options:nil];
            cell = club_cell;
            self.club_cell = nil;
        }
        Club *club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath:indexPath].row];
        
        // Configure the cell...
        
        UILabel *label;
        label = (UILabel *)[cell viewWithTag:1];
        label.text = club.title;
        
        label = (UILabel *)[cell viewWithTag:2];
        label.text = club.subtitle;
        
        UIImageView *iconImage;
        iconImage = (UIImageView *)[cell viewWithTag:3];
        NSRange range = [club.title rangeOfString:@"Coed"];
        iconImage.image = (range.location == NSNotFound) ? [UIImage imageNamed:@"women.png"]:[UIImage imageNamed:@"coed.png"];
        if ([self.selected_index_path isEqual:indexPath]) {
            cell.backgroundView= [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"club_cell_bg_selected.png"]] autorelease];
        }
        else {
            cell.backgroundView= [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"club_cell_bg.png"]] autorelease];
        }

        
        
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.clubs_list_view){
        if([indexPath isEqual:self.control_row_index_path]){
            return 50; //height for control cell
        }
    }
    return 66;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.clubs_list_view){
        
        // set cell background image
//        UITableViewCell* cell = [tableView cellForRowAtIndexPath: self.selected_index_path];
//        cell.backgroundView= [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"club_cell_bg.png"]] autorelease];
        
        //if user tapped the same row twice let's start getting rid of the control cell
        if([indexPath isEqual:self.selected_index_path]){
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
        
        //update the indexpath if needed... I explain this below 
        NSIndexPath *updatedIndexPath =  [self modelIndexPathforIndexPath:indexPath];
        
        //pointer to delete the control cell
        NSIndexPath *indexPathToDelete = [NSIndexPath indexPathForRow:self.control_row_index_path.row inSection:self.control_row_index_path.section];
        
        //if in fact I tapped the same row twice lets clear our tapping trackers 
        if([updatedIndexPath isEqual:self.selected_index_path]){
            self.selected_index_path = nil;
            self.control_row_index_path = nil;        
        }
        //otherwise let's update them appropriately 
        else{
            self.selected_index_path = updatedIndexPath; //the row the user just tapped. 
            //Now I set the location of where I need to add the dummy cell 
            self.control_row_index_path = [NSIndexPath indexPathForRow:updatedIndexPath.row + 1   inSection:updatedIndexPath.section];
        }
        
        //all logic is done, lets start updating the table
        [tableView beginUpdates];
        
        //lets delete the control cell, either the user tapped the same row twice or tapped another row
        if(indexPathToDelete.row != 0){
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            UITableViewCell* cell = [tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:indexPathToDelete.row - 1   inSection:indexPathToDelete.section]];
            cell.backgroundView= [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"club_cell_bg.png"]] autorelease];
        }
        //lets add the new control cell in the right place 
        if(self.control_row_index_path){
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:self.control_row_index_path] 
                             withRowAnimation:UITableViewRowAnimationNone];
            // set selected cell background image
            UITableViewCell* cell = [tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:indexPath.row inSection: self.control_row_index_path.section]];
            cell.backgroundView= [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"club_cell_bg_selected.png"]] autorelease];
        }
        
        
        
        //and we are done... 
        [tableView endUpdates];
        if(self.control_row_index_path.row == self.clubs.count){
            [tableView scrollToRowAtIndexPath:self.control_row_index_path atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        
    }
}

- (void) ClubOptionsNavigateBtnTapped:(id)sender{
    Club* club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath: self.control_row_index_path].row];
    [self getClubDirections:club];
}

- (void) ClubOptionsCallBtnTapped:(id) sender{
    Club* club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath: self.control_row_index_path].row];
    [self getClubPhoneNumber:club];
}

- (void) ClubOptionsSchedulesBtnTapped:(id) sender{
    
    Club* club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath: self.control_row_index_path].row];
    [self getClubSchedules:club];
}

- (void) ClubOptionsDetailsBtnTapped:(id) sender{
    Club* club = [self.clubs objectAtIndex:[self modelIndexPathforIndexPath: self.control_row_index_path].row];
    [self getClubDetails:club];
}

#pragma mark - MapViewDelegates

-(void)mapViewWillStartLocatingUser:(MKMapView *)mapView{
    [self showHud];
    [mapView removeAnnotations:mapView.annotations];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if(userLocation.coordinate.latitude == 0.0f && userLocation.coordinate.longitude == 0.0f){
        // we haven't locate the user yet, do nothing
    }
    else{
        [FlurryAnalytics setLatitude:mapView.userLocation.coordinate.latitude longitude:mapView.userLocation.coordinate.longitude 
                  horizontalAccuracy:userLocation.location.horizontalAccuracy verticalAccuracy:userLocation.location.verticalAccuracy];
        self.currentLocation = mapView.userLocation;    // set currentLocation
        [self zoomMapViewToCoordinate:userLocation.coordinate];    // zoom to user location
        mapView.showsUserLocation = NO;    // stop locating user
    }
}

-(void) mapViewDidStopLocatingUser:(MKMapView *)mapView{
    if(self.currentLocation != nil){
        [mapView addAnnotation: self.currentLocation];    // add annotation to user location
        [serviceManager getClubsByLocation:self.currentLocation.coordinate within:10 sender:self];    // request for clubs
    }
}

-(void) mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    NSLog(@"%@",error.description);
    mapView.showsUserLocation = NO;    // stop locating user
    NSString *message = nil;
    switch (error.code) {
        case kCLErrorLocationUnknown:
            // unknown
            message = @"Please make sure location services are enabled for this app, then try again by tapping the locate button on top left corner. \nThanks!";
            break;
        case kCLErrorDenied:
        default:
            // denined
            message = @"Looks like location services have been turned off for this app. You can enabled it by go to Settings -> General -> Location Services to turn it on. \nThanks!";
            break;
    }
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"We could not locate you :(" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
    alert.tag = 0;
    [alert show];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
//	if (view.annotation == self.customAnnotation) {
    if ([view.annotation isKindOfClass:[Club class]]){
		if (self.calloutAnnotation == nil) {
			self.calloutAnnotation = [[[CalloutMapAnnotation alloc] initWithLatitude:view.annotation.coordinate.latitude
																	   andLongitude:view.annotation.coordinate.longitude] autorelease];
		} else {
			self.calloutAnnotation.latitude = view.annotation.coordinate.latitude;
			self.calloutAnnotation.longitude = view.annotation.coordinate.longitude;
		}
		[self.clubs_map_view addAnnotation:self.calloutAnnotation];
		self.selectedAnnotationView = view;
	}
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
	if (self.calloutAnnotation && 
		[view.annotation isKindOfClass:[Club class]] && 
		!((BasicMapAnnotationView *)view).preventSelectionChange) {
		[self.clubs_map_view removeAnnotation: self.calloutAnnotation];
	}
}

-(void) mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views{
    double delay = 0.0;
    CGRect visibleRect = [mapView annotationVisibleRect];
    for (MKAnnotationView *view in views) {
        if(![view isKindOfClass:[AccessorizedCalloutMapAnnotationView class]]){
            CGRect endFrame = view.frame;
            CGRect startFrame = endFrame;
            startFrame.origin.y = visibleRect.origin.y - startFrame.size.height;
            view.frame = startFrame;
            [UIView beginAnimations:@"drop" context:NULL];
            [UIView setAnimationDuration:0.3];
            [UIView setAnimationDelay:delay];
            view.frame = endFrame;
            [UIView commitAnimations];
            delay += 0.1;
        }
    }
}


- (MKAnnotationView *) mapView:(MKMapView *) mapView viewForAnnotation:(id ) annotation {
    if (annotation == self.calloutAnnotation) {
		CalloutMapAnnotationView *calloutMapAnnotationView = nil;//(CalloutMapAnnotationView *)[self.clubs_map_view dequeueReusableAnnotationViewWithIdentifier:@"CalloutAnnotation"];
		if (!calloutMapAnnotationView) {
			calloutMapAnnotationView = [[[AccessorizedCalloutMapAnnotationView alloc] initWithAnnotation:annotation 
                                                                                         reuseIdentifier:@"CalloutAnnotation"] autorelease];
			calloutMapAnnotationView.contentHeight = 126.0f;
            NSRange range = [((Club *)self.selectedAnnotationView.annotation).title rangeOfString:@"Coed"];
            UIImage *asynchronyLogo = (range.location == NSNotFound) ? [UIImage imageNamed:@"women.png"]:[UIImage imageNamed:@"coed.png"];
			UIImageView *asynchronyLogoView = [[[UIImageView alloc] initWithImage:asynchronyLogo] autorelease];
			asynchronyLogoView.frame = CGRectMake(5, 5, asynchronyLogoView.frame.size.width, asynchronyLogoView.frame.size.height);
            UILabel *label = (UILabel*)[calloutMapAnnotationView viewWithTag:5];
            label.text = ((Club *)self.selectedAnnotationView.annotation).title;
            label = (UILabel*)[calloutMapAnnotationView viewWithTag:6];
            label.text = ((Club *)self.selectedAnnotationView.annotation).subtitle;
			[calloutMapAnnotationView.contentView addSubview:asynchronyLogoView];
		}
		calloutMapAnnotationView.parentAnnotationView = self.selectedAnnotationView;
		calloutMapAnnotationView.mapView = self.clubs_map_view;
		return calloutMapAnnotationView;
	} 
    else if([annotation isKindOfClass:[Club class]]) {
        
        NSString *club_name = ((Club*)annotation).name;
        MKPinAnnotationView *pin = (BasicMapAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: club_name];
        if (pin == nil) {
            pin = [[[BasicMapAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:club_name] autorelease];
        } else {
            pin.annotation = annotation;
        }
        pin.pinColor = MKPinAnnotationColorRed;
        pin.animatesDrop = YES;
        pin.canShowCallout = NO;
        return pin;
    }
    else {
        MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: @"user_location"];
        if (pin == nil) {
            pin = [[[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"user_location"] autorelease];
        } else {
            pin.annotation = annotation;
        }
        pin.pinColor = MKPinAnnotationColorGreen;
        pin.animatesDrop = YES;
        pin.canShowCallout = YES;
        return pin;
    }
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    if ([view.annotation isKindOfClass:[Club class]]){
        NSLog(@"tag: %d", control.tag);
        Club* club = (Club *)self.selectedAnnotationView.annotation;
        switch (control.tag) {
            case 1:
                [self getClubDirections:club];
                break;
                
            case 2:
                [self getClubPhoneNumber:club];
                break;
            case 3:
                [self getClubSchedules:club];
                break;
            case 4:
                [self getClubDetails:club];
                break;
                
            default:
                break;
        }
    }
}

-(void) zoomMapViewToCoordinate:(CLLocationCoordinate2D)coordinate{
    
    MKCoordinateRegion region;
    region.center = coordinate;  
    
    MKCoordinateSpan span; 
    span.latitudeDelta  = 0.1; // Change these values to change the zoom
    span.longitudeDelta = 0.1; 
    region.span = span;
    
    [self.clubs_map_view setRegion:region animated:YES];
    
}

#pragma mark - IBActions

-(IBAction)locateNearByClubs{
    self.clubs_map_view.showsUserLocation = YES;
    [self hideSearchView];
}
-(IBAction)showMapView{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.pointer.frame = CGRectMake(16, self.pointer.frame.origin.y, self.pointer.frame.size.width, self.pointer.frame.size.height);
    self.clubs_list_view.alpha = 0;
    self.clubs_map_view.alpha=1;
    [UIView commitAnimations];
    
}
-(IBAction)showListView{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.pointer.frame = CGRectMake(66, self.pointer.frame.origin.y, self.pointer.frame.size.width, self.pointer.frame.size.height);
    self.clubs_list_view.alpha=1;
    self.clubs_map_view.alpha=0;
    [UIView commitAnimations];
}
-(IBAction)showSearchView{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.search_view.frame = CGRectMake(search_view.frame.origin.x, 0, search_view.frame.size.width, search_view.frame.size.height);
    [self.search_text_field becomeFirstResponder];
    [UIView commitAnimations];
}
-(IBAction)hideSearchView{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.search_text_field.text = @"";
    [self.search_text_field resignFirstResponder];
    self.search_view.frame = CGRectMake(search_view.frame.origin.x, -416, search_view.frame.size.width, search_view.frame.size.height);
    [UIView commitAnimations];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.clubs_map_view removeAnnotations:self.clubs_map_view.annotations];
    [self.serviceManager getClubsByAddress:textField.text within:10 sender:self];
    [self hideSearchView];
    [self showHud];
    return YES;
}

#pragma mark - UIAlertViewDelegate

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case 2: // phone
            if (buttonIndex == 1) {
                NSString *number = [alertView.message substringWithRange:NSMakeRange(5, 13)];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", number]]];
            }
            break;
        case 1: // bad search address/postal code
            if (buttonIndex == 0) {
                [self showSearchView];
            }
            break;
        case 0: // location
            [HUD hide:YES];
            break;
        default:
            break;
    }
}

#pragma mark - ASIHTTPRequestDelegate

- (void)requestFinished:(ASIHTTPRequest *)request
{
    // parse json to clubs
    @try {
        
        self.clubs = [ModelObjectFactory ClubsFromJSONString:request.responseString];
        // add annotation to the searched address centre
        if([[request.userInfo objectForKey:@"request_id"] isEqualToString:@"getClubsByAddress"]){
            NSDictionary *centre = [ModelObjectFactory mapCentreFromClubSearchResult:request.responseString];
            CLLocationCoordinate2D centre_coordinate = CLLocationCoordinate2DMake([[centre objectForKey:@"lat"] floatValue], [[centre objectForKey:@"lng"] floatValue]);
            MKPointAnnotation *centre_point = [[[MKPointAnnotation alloc] init] autorelease];
            centre_point.coordinate = centre_coordinate;
            centre_point.title = [centre objectForKey:@"address"];
            [self.clubs_map_view addAnnotation:centre_point];
            [self zoomMapViewToCoordinate:centre_coordinate];
        }
    }
    @catch (NSException *exception) {
        self.clubs = nil;
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"We could not find the address/postal code you searched for. Please try another address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        alert.tag = 1;
        [alert show];
    }
    @finally {
        // add annotations 
        for( Club *club in self.clubs){
            [self.clubs_map_view addAnnotation:club];   
        }
        // refresh clubs table view
        self.selected_index_path = nil;
        self.control_row_index_path = nil;
        [UIView beginAnimations:@"hide_clubs_list" context:NULL];
        [UIView setAnimationDuration:1];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.clubs_list_view cache:NO];
        [UIView commitAnimations];
        [self.clubs_list_view reloadData];
        
        [request.delegate release];
        [HUD hide:YES];
    }


}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [HUD hide:YES];
    [FlurryAnalytics logError:@"request_error" message:request.url.absoluteString error:request.error];
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Oops..." message:@"We lost what you were looking for on the way. Would you like me to make another trip?" delegate:self cancelButtonTitle:@"No it's ok" otherButtonTitles:@"Yes try again", nil] autorelease];
    alert.tag = 0;
    [alert show];
    [request.delegate release];
}

#pragma mark MBProgressHUDDelegate methods

- (void)showHud{

    HUD = [[[MBProgressHUD alloc] initWithView:self.navigationController.view] autorelease];

    HUD.delegate = self;
    HUD.labelText = @"Loading Clubs";
//    HUD.detailsLabelText = @"I am finding GoodLife clubs near by";
    [HUD show:YES];
	[self.navigationController.view addSubview:HUD];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
	HUD = nil;
}

#pragma mark - private methods
-(void) getClubDirections:(Club *)club{
    [FlurryAnalytics logEvent:@"Ged Directions"];
    NSString* url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%f,%f&daddr=%@",
                                                        self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude,
                                                        [club.subtitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];

}
-(void) getClubPhoneNumber:(Club *)club{
    [FlurryAnalytics logEvent:@"Get Club Phone Number"];
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat: @"Call %@?",[club.phone stringByReplacingOccurrencesOfString:@" " withString:@""]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
        [alert show];
        alert.tag = 2;
        [alert release];
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat: @"Sorry, you device can not make phone calls but here is the number: %@", club.phone ] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
        [Notpermitted release];
    }
}
-(void) getClubSchedules:(Club *)club{
    [FlurryAnalytics logEvent:@"Ged Glub Schedules"];
    if(club.schedules.count <= 1){

        ScheduleDetailsViewController *scheduleDetailsController = [[[ScheduleDetailsViewController alloc] initWithNibName:@"ScheduleDetailsViewController" bundle:nil] autorelease];
        scheduleDetailsController.schedule = [club.schedules objectAtIndex:0];
        scheduleDetailsController.club = club;
        [self.navigationController pushViewController:scheduleDetailsController animated:YES];
        return;
    }
    else{    
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Group Exercise Schedules"];

        for (Schedule *schedule in club.schedules){
            [sheet addButtonWithTitle:schedule.name block:^{
                    ScheduleDetailsViewController *scheduleDetailsController = [[[ScheduleDetailsViewController alloc] initWithNibName:@"ScheduleDetailsViewController" bundle:nil] autorelease];
                    scheduleDetailsController.schedule = schedule;
                    scheduleDetailsController.club = club;
                    [self.navigationController pushViewController:scheduleDetailsController animated:YES];
                }
             ];
        }
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
    }
}
-(void) getClubDetails:(Club *)club{
    [FlurryAnalytics logEvent:@"Ged Club Details"];
    ClubDetailsViewController *clubsDetailsController = [[[ClubDetailsViewController alloc] initWithNibName:@"ClubDetailsViewController" bundle:nil] autorelease];
    clubsDetailsController.club = club;
    clubsDetailsController.currentLocationCoordinate = self.currentLocation.coordinate;
    [self.navigationController pushViewController:clubsDetailsController animated:YES];
}

@end
