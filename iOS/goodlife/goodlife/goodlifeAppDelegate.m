//
//  goodlifeAppDelegate.m
//  goodlife
//
//  Created by Brian Ge on 11-10-26.
//  Copyright 2011 T4G. All rights reserved.
//

#import "goodlifeAppDelegate.h"
#import "SCAppUtils.h"
#import "ClubsViewController.h"
#import "TestFlight.h"
#import "FlurryAnalytics.h"

@implementation goodlifeAppDelegate


@synthesize window=_window;

//@synthesize viewController=_viewController;

void uncaughtExceptionHandler(NSException *exception) {
    [FlurryAnalytics logError:@"Uncaught" message:@"Crash!" exception:exception];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
#if !TARGET_IPHONE_SIMULATOR
    [FlurryAnalytics startSession:@"1F5BJEZWJRDZGP8U46ZM"];
    [TestFlight takeOff:@"70010c4006f79f6913dbdc4b2515e47a_NTk2MTAyMDEyLTAyLTA0IDE0OjA4OjA5LjgyNzQ5Nw"];
#endif
    // Override point for customization after application launch.
    ClubsViewController *rootViewController = [[[ClubsViewController alloc] init] autorelease];
    [FlurryAnalytics logAllPageViews:rootViewController];
    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:rootViewController] autorelease];
    [SCAppUtils customizeNavigationController:navController];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)dealloc
{
    [_window release];
    //[_viewController release];
    [super dealloc];
}


@end
