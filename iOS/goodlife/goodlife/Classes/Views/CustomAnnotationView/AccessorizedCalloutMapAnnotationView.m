#import "AccessorizedCalloutMapAnnotationView.h"
#import "BasicMapAnnotationView.h"

@interface AccessorizedCalloutMapAnnotationView()

@property (nonatomic, retain) UIButton *accessory1;
@property (nonatomic, retain) UIButton *accessory2;
@property (nonatomic, retain) UIButton *accessory3;
@property (nonatomic, retain) UIButton *accessory4;
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *subtitle;

@end


@implementation AccessorizedCalloutMapAnnotationView

@synthesize accessory1 = _accessory1;
@synthesize accessory2 = _accessory2;
@synthesize accessory3 = _accessory3;
@synthesize accessory4 = _accessory4;
@synthesize title = _title;
@synthesize subtitle = _subtitle;

- (id) initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
	if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
		self.accessory1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.accessory1 setBackgroundImage:[UIImage imageNamed:@"tools cell_button"] forState:UIControlStateNormal];
        [self.accessory1 setBackgroundImage:[UIImage imageNamed:@"tools cell_button_highlighted"] forState:UIControlStateHighlighted];
        [self.accessory1 setImage:[UIImage imageNamed:@"directions"] forState:UIControlStateNormal];
		self.accessory1.exclusiveTouch = YES;
        self.accessory1.tag = 1;
		self.accessory1.enabled = YES;
		[self.accessory1 addTarget: self 
						   action: @selector(calloutAccessoryTapped:) 
				 forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchCancel];
		[self addSubview:self.accessory1];
        self.accessory2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.accessory2 setBackgroundImage:[UIImage imageNamed:@"tools cell_button"] forState:UIControlStateNormal];
        [self.accessory2 setBackgroundImage:[UIImage imageNamed:@"tools cell_button_highlighted"] forState:UIControlStateHighlighted];
        [self.accessory2 setImage:[UIImage imageNamed:@"phone"] forState:UIControlStateNormal];
		self.accessory2.exclusiveTouch = YES;
        self.accessory2.tag = 2;
		self.accessory2.enabled = YES;
		[self.accessory2 addTarget: self 
                            action: @selector(calloutAccessoryTapped:) 
                  forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchCancel];
		[self addSubview:self.accessory2];
        self.accessory3 = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.accessory3 setBackgroundImage:[UIImage imageNamed:@"tools cell_button"] forState:UIControlStateNormal];
        [self.accessory3 setBackgroundImage:[UIImage imageNamed:@"tools cell_button_highlighted"] forState:UIControlStateHighlighted];
        [self.accessory3 setImage:[UIImage imageNamed:@"calendars"] forState:UIControlStateNormal];
		self.accessory3.exclusiveTouch = YES;
        self.accessory3.tag = 3;
		self.accessory3.enabled = YES;
		[self.accessory3 addTarget: self 
                            action: @selector(calloutAccessoryTapped:) 
                  forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchCancel];
		[self addSubview:self.accessory3];
        self.accessory4 = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.accessory4 setBackgroundImage:[UIImage imageNamed:@"tools cell_button"] forState:UIControlStateNormal];
        [self.accessory4 setBackgroundImage:[UIImage imageNamed:@"tools cell_button_highlighted"] forState:UIControlStateHighlighted];
        [self.accessory4 setImage:[UIImage imageNamed:@"details"] forState:UIControlStateNormal];
		self.accessory4.exclusiveTouch = YES;
        self.accessory4.tag = 4;
		self.accessory4.enabled = YES;
		[self.accessory4 addTarget: self 
                            action: @selector(calloutAccessoryTapped:) 
                  forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchCancel];
		[self addSubview:self.accessory4];
        self.title = [[[UILabel alloc] init] autorelease];
        self.title.tag = 5;
        self.title.text = @"title label";
        self.title.numberOfLines = 2;
        self.title.adjustsFontSizeToFitWidth=NO;
        self.title.lineBreakMode = UILineBreakModeWordWrap;
        self.title.textColor = [UIColor whiteColor];
        self.title.backgroundColor = [UIColor clearColor];
        self.title.font = [UIFont fontWithName:@"Helvetica" size:15.0f];
        [self addSubview:self.title];
        self.subtitle = [[[UILabel alloc] init] autorelease];
        self.subtitle.tag = 6;
        self.subtitle.adjustsFontSizeToFitWidth=NO;
        self.subtitle.text = @"subtitle label";
        self.subtitle.textColor = [UIColor whiteColor];
        self.subtitle.backgroundColor = [UIColor clearColor];
        self.subtitle.font = [UIFont fontWithName:@"Helvetica-Oblique" size:11.0f];
        [self addSubview:self.subtitle];
	}
	return self;
}

- (void)prepareContentFrame {
	CGRect contentFrame = CGRectMake(self.bounds.origin.x + 10, 
									 self.bounds.origin.y + 3, 
									 self.bounds.size.width - 20, 
									 self.contentHeight);
	
	self.contentView.frame = contentFrame;
}

- (void)prepareAccessoryFrame {
//    self.accessory.frame = CGRectMake(self.bounds.size.width - self.accessory.frame.size.width - 15, 
//									  (self.contentHeight + 3 - self.accessory.frame.size.height) / 2, 
//									  self.accessory.frame.size.width, 
//									  self.accessory.frame.size.height);
	self.accessory1.frame = CGRectMake(26, 
									  self.contentView.frame.size.height - 44, 
									  44, 
                                       33);
	self.accessory2.frame = CGRectMake(self.accessory1.frame.origin.x + self.accessory1.frame.size.width + 20, 
                                       self.contentView.frame.size.height - 44, 
                                       44, 
                                       33);
	self.accessory3.frame = CGRectMake(self.accessory2.frame.origin.x + self.accessory2.frame.size.width + 20, 
                                       self.contentView.frame.size.height - 44, 
                                       44, 
                                       33);
	self.accessory4.frame = CGRectMake(self.accessory3.frame.origin.x + self.accessory3.frame.size.width + 20, 
                                       self.contentView.frame.size.height - 44, 
                                       44, 
                                       33);
    self.title.frame = CGRectMake(80, 3, self.bounds.size.width - 80 - 10, 40);
    self.subtitle.frame = CGRectMake(80, 45, self.bounds.size.width - 80 - 10, 15);
}

- (void)didMoveToSuperview {
	[super didMoveToSuperview];
	[self prepareAccessoryFrame];
}

- (void) calloutAccessoryTapped: (id) sender {
	if ([self.mapView.delegate respondsToSelector:@selector(mapView:annotationView:calloutAccessoryControlTapped:)]) {
		[self.mapView.delegate mapView:self.mapView 
						annotationView:self.parentAnnotationView 
		 calloutAccessoryControlTapped:sender];
	}
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	
	UIView *hitView = [super hitTest:point withEvent:event];
	
	//If the accessory is hit, the map view may want to select an annotation sitting below it, so we must disable the other annotations
	//But not the parent because that will screw up the selection
	if (hitView == self.accessory1 || hitView == self.accessory2 || hitView == self.accessory3 || hitView == self.accessory4) {
		[self preventParentSelectionChange];
		[self performSelector:@selector(allowParentSelectionChange) withObject:nil afterDelay:1.0];
		for (UIView *sibling in self.superview.subviews) {
			if ([sibling isKindOfClass:[MKAnnotationView class]] && sibling != self.parentAnnotationView) {
				((MKAnnotationView *)sibling).enabled = NO;
				[self performSelector:@selector(enableSibling:) withObject:sibling afterDelay:1.0];
			}
		}
	}
	
	return hitView;
}

- (void) enableSibling:(UIView *)sibling {
	((MKAnnotationView *)sibling).enabled = YES;
}

- (void) preventParentSelectionChange {
	BasicMapAnnotationView *parentView = (BasicMapAnnotationView *)self.parentAnnotationView;
	parentView.preventSelectionChange = YES;
}

- (void) allowParentSelectionChange {
	//The MapView may think it has deselected the pin, so we should re-select it
	[self.mapView selectAnnotation:self.parentAnnotationView.annotation animated:NO];
	
	BasicMapAnnotationView *parentView = (BasicMapAnnotationView *)self.parentAnnotationView;
	parentView.preventSelectionChange = NO;
}

@end
