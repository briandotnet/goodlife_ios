#import <Foundation/Foundation.h>
#import "CalloutMapAnnotationView.h"

@interface AccessorizedCalloutMapAnnotationView : CalloutMapAnnotationView {
	UIButton *_accessory1;
    UIButton *_accessory2;
	UIButton *_accessory3;
    UIButton *_accessory4;
    UILabel *_title;
    UILabel *_subtitle;
}

- (void) calloutAccessoryTapped: (id) sender;
- (void) preventParentSelectionChange;
- (void) allowParentSelectionChange;
- (void) enableSibling:(UIView *)sibling;

@end
