//
//  UIBarButtonItem.h
//  goodlife
//
//  Created by Brian Ge on 12-01-28.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <Foundation/Foundation.h>

// UIBarButtonItem+Image.h

@interface UIBarButtonItem (Image)

-(id)initWithImage:(UIImage *)image title:(NSString*)title target:(id)target action:(SEL)action;
//-(void)setEnabled:(BOOL)enabled ;
@end