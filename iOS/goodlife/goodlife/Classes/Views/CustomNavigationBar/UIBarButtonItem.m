//
//  UIBarButtonItem.m
//  goodlife
//
//  Created by Brian Ge on 12-01-28.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "UIBarButtonItem.h"

//  UIBarButtonItem+Image.m

@implementation UIBarButtonItem (Image)

-(id)initWithImage:(UIImage *)image title:(NSString*)title target:(id)target action:(SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame= CGRectMake(0.0, 0.0, 48, 32);
    button.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    button.titleLabel.shadowOffset = CGSizeMake(0, -1);
    button.titleLabel.shadowColor = [UIColor colorWithWhite:0 alpha:0.5];
    
    [button showsTouchWhenHighlighted];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:image forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIView *view =[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 48, 32) ];
    [view addSubview:button];
    
    self = [[UIBarButtonItem alloc] initWithCustomView:view];
    
    [view release];
    
    return self;
}

//-(void)setEnabled:(BOOL)enabled {
//    if (self.customView) {
//        if ([[self.customView.subviews objectAtIndex:0] isKindOfClass:[UIButton class]]) {
//            ((UIButton*)[self.customView.subviews objectAtIndex:0]).enabled = enabled;
//        }
//    }
//    else{
//        self.enabled = enabled;
//    }
//}

@end