//
//  GroupClass.m
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "GroupClass.h"

@implementation GroupClass

@synthesize group_class_id, group_class_type_id, name, icon, start_time, duration, day_of_week, day_of_week_desc;

- (void)dealloc {
    [group_class_id release];
    [group_class_type_id release];
	[name release];
    [icon release];
	[start_time release];
	[duration release];
	[day_of_week release];
	[day_of_week_desc release];
    [super dealloc];
}

- (NSDate *)end_time{
    return [start_time dateByAddingTimeInterval:duration.intValue*60];
}
- (NSComparisonResult) compareByStartTime:(GroupClass*) group_class{
    return [self.start_time compare:group_class.start_time];
}

@end
