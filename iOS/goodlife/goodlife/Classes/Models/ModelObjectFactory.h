//
//  ModelObjectFactory.h
//  goodlife
//
//  Created by Brian Ge on 12-01-23.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"
#import "Club.h"
#import "Schedule.h"
#import "GroupClass.h"

@interface ModelObjectFactory : NSObject

+ (void) CheckForError:(NSDictionary *) dictionary;

+ (NSArray *) ClubsFromJSONString:(NSString *) JSONString;
//+ (NSArray *) SchedulesFromJSONString:(NSString *) JSONString;
+ (Schedule *) ScheduleFromJSONString:(NSString *) JSONString;


+ (Club *) ClubFromDictionary:(NSDictionary *) dictionary;
+ (Schedule *) ScheduleFromDictionary:(NSDictionary *) dictionary;
+ (GroupClass *) GroupClassFromDictionary:(NSDictionary *) dictionary;
+ (NSDictionary * ) mapCentreFromClubSearchResult:(NSString *) JSONString;

@end
