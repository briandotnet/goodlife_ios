//
//  Club.m
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "Club.h"

@implementation Club

@synthesize club_id, name, address_line_1, address_line_2, city, province, postal_code, phone, email, url, schedules;
@synthesize latitude, longitude, distance;


- (CLLocationCoordinate2D) coordinate{
    return CLLocationCoordinate2DMake(self.latitude, self.longitude);
}

- (NSString *) title{
    return name;
}
- (NSString *) subtitle{

    if ([address_line_2 isEqual:[NSNull null]]){
        return [NSString stringWithFormat:@"%@, %@", address_line_1, city];
    }
    return [NSString stringWithFormat:@"%@, %@, %@", address_line_1, address_line_2, city]; 
}


- (void)dealloc {
    [club_id release];
	[name release];
	[address_line_1 release];
    [address_line_2 release];
	[city release];
	[province release];
	[postal_code release];
	[phone release];
    [email release];
	[url release];
    [schedules release];
	[super dealloc];
}

@end
