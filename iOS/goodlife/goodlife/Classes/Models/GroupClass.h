//
//  GroupClass.h
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupClass : NSObject{
    
    NSNumber *group_class_id;
    NSNumber *group_class_type_id;
	NSString *name;
    NSString *icon;
	NSDate *start_time;
    NSNumber *duration;
    NSNumber *day_of_week;
    NSString *day_of_week_desc;
}

@property (nonatomic, retain) NSNumber *group_class_id;
@property (nonatomic, retain) NSNumber *group_class_type_id;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSDate *start_time;
@property (nonatomic, retain) NSNumber *duration;
@property (nonatomic, retain) NSNumber *day_of_week;
@property (nonatomic, retain) NSString *day_of_week_desc;

- (NSDate *)end_time;
- (NSComparisonResult) compareByStartTime:(GroupClass*) group_class;

@end
