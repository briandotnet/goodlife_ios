//
//  Schedule.h
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Schedule : NSObject{
    
    NSNumber *schedule_id;
    NSString *name;
	NSDate *start_date;
	NSDate *end_date;
    NSString *url;
    BOOL is_active;
    BOOL is_current;
    NSDictionary *group_classes;
}

@property (nonatomic, retain) NSNumber *schedule_id;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSDate *start_date;
@property (nonatomic, retain) NSDate *end_date;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, assign) BOOL is_active;
@property (nonatomic, assign) BOOL is_current;
@property (nonatomic, retain) NSDictionary *group_classes;

@end
