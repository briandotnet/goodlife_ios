//
//  Club.h
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Schedule.h"

@interface Club : NSObject<MKAnnotation>{
    NSNumber *club_id;
    NSString *name;
	NSString *address_line_1;
	NSString *address_line_2;
	NSString *city;
	NSString *province;
	NSString *postal_code;
	NSString *phone;
    NSString *email;
	NSString *url;    
    NSArray *schedules;
    
	CLLocationDegrees latitude;
	CLLocationDegrees longitude;
    double distance;

}

@property (nonatomic, retain) NSNumber *club_id;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *address_line_1;
@property (nonatomic, retain) NSString *address_line_2;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *province;
@property (nonatomic, retain) NSString *postal_code;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSArray *schedules;

@property (nonatomic, assign) CLLocationDegrees latitude;
@property (nonatomic, assign) CLLocationDegrees longitude;
@property (nonatomic, assign) double distance;

// MKAnnotation Protocol
- (CLLocationCoordinate2D) coordinate;
- (NSString *) title;
- (NSString *) subtitle;

@end
