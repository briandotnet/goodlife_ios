//
//  ModelObjectFactory.m
//  goodlife
//
//  Created by Brian Ge on 12-01-23.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "ModelObjectFactory.h"
@implementation ModelObjectFactory

+ (void) CheckForError:(NSDictionary *) dictionary{
    if([dictionary objectForKey:@"error"]){
        [NSException raise:@"Error in Server Response" format:[dictionary objectForKey:@"error"]];
    }
}

+ (NSDictionary * ) mapCentreFromClubSearchResult:(NSString *) JSONString{
    
    NSError *error;
	SBJsonParser *json = [[[SBJsonParser alloc] init] autorelease];
    NSDictionary *response_dictionary = [json objectWithString:JSONString error:&error];
    [self CheckForError:response_dictionary];
    return [response_dictionary objectForKey:@"centre"];

}

+ (NSArray *) ClubsFromJSONString:(NSString *) JSONString{
    NSError *error;
	SBJsonParser *json = [[[SBJsonParser alloc] init] autorelease];
    NSMutableArray *clubs = [[[NSMutableArray alloc] init] autorelease];
    NSDictionary *response_dictionary = [json objectWithString:JSONString error:&error];
    [self CheckForError:response_dictionary];
	NSArray *clubs_collection = [response_dictionary objectForKey:@"clubs"] ;
    for (NSDictionary *one_club in clubs_collection) {
        Club *aClub = [ModelObjectFactory ClubFromDictionary:[one_club objectForKey:@"club"]];
        [clubs addObject:aClub];
    }
    
    return clubs;
}

//+ (NSArray *) SchedulesFromJSONString:(NSString *) JSONString{
//    
//    NSError *error;
//	SBJsonParser *json = [[[SBJsonParser alloc] init] autorelease];
//    NSMutableArray *schedules = [[[NSMutableArray alloc] init] autorelease];
//	NSArray *schedules_collection = [json objectWithString:JSONString error:&error] ;
//    [self CheckForError:response_dictionary];
//    for (NSDictionary *one_schedule in schedules_collection){
//        Schedule *aSchedule = [ModelObjectFactory ScheduleFromDictionary:[one_schedule objectForKey:@"schedule"]];
//        [schedules addObject:aSchedule];
//    }
//    return schedules;
//}

+ (Schedule *) ScheduleFromJSONString:(NSString *) JSONString{
    NSError *error;
	SBJsonParser *json = [[[SBJsonParser alloc] init] autorelease];
	NSDictionary *schedule_dictionary = [json objectWithString:JSONString error:&error] ;
    [self CheckForError:schedule_dictionary];
    Schedule *schedule = [ModelObjectFactory ScheduleFromDictionary:[schedule_dictionary objectForKey:@"schedule"]];
    
    return schedule;

}


+ (Club *) ClubFromDictionary:(NSDictionary *) dictionary{
    
    Club *club  = [[[Club alloc] init] autorelease];
    
    club.club_id = [dictionary objectForKey:@"id"];
    club.name = [dictionary objectForKey:@"name"];
    club.address_line_1 = [dictionary objectForKey:@"address_line_1"];
    club.address_line_2 = [dictionary objectForKey:@"address_line_2"];
    club.city = [dictionary objectForKey:@"city"];
    club.province = [dictionary objectForKey:@"province"];
    club.postal_code = [dictionary objectForKey:@"postal_code"];
    club.phone = [dictionary objectForKey:@"phone"];
    club.email = [dictionary objectForKey:@"email"];
    club.url = [dictionary objectForKey:@"gl_club_url"];
    club.latitude = [[dictionary objectForKey:@"lat"] floatValue];
    club.longitude = [[dictionary objectForKey:@"lng"]floatValue];
    club.distance = [[dictionary objectForKey:@"distance"] floatValue];
    
    NSMutableArray *schedules = [[[NSMutableArray alloc] init] autorelease];
    for (NSDictionary *one_schedule in [dictionary objectForKey:@"schedules"]) {
        [schedules addObject:[ModelObjectFactory ScheduleFromDictionary:one_schedule]];
    }
    club.schedules = schedules;
    
    return club;
}
+ (Schedule *) ScheduleFromDictionary:(NSDictionary *) dictionary{
    
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];

    Schedule *schedule = [[[Schedule alloc] init] autorelease];
    
    schedule.schedule_id = [dictionary objectForKey:@"id"];
    schedule.name = [dictionary objectForKey:@"name"];
    schedule.start_date = [dateFormatter dateFromString: [dictionary objectForKey:@"start_date"]];
    schedule.end_date = [dateFormatter dateFromString: [dictionary objectForKey:@"end_date"]];
    schedule.url = [dictionary objectForKey:@"gl_schedule_url"];
    schedule.is_active = [[dictionary objectForKey:@"is_active"] boolValue];
    schedule.is_current = [[dictionary objectForKey:@"is_current?"] boolValue];
    
    NSMutableDictionary *group_classes = [[[NSMutableDictionary alloc] init] autorelease];
    
    if([dictionary objectForKey:@"group_classes"]){
        NSMutableArray* mon = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray* tue = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray* wed = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray* thu = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray* fri = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray* sat = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray* sun = [[[NSMutableArray alloc] init] autorelease];
        
        for (NSDictionary *one_class in [dictionary objectForKey:@"group_classes"]) {
            GroupClass *group_class =[ModelObjectFactory GroupClassFromDictionary:one_class]; 
            switch (group_class.day_of_week.intValue) {
                case 0:
                    [mon addObject:group_class];
                    break;
                case 1:
                    [tue addObject:group_class];
                    break;
                case 2:
                    [wed addObject:group_class];
                    break;
                case 3:
                    [thu addObject:group_class];
                    break;
                case 4:
                    [fri addObject:group_class];
                    break;
                case 5:
                    [sat addObject:group_class];
                    break;
                case 6:
                    [sun addObject:group_class];
                    break;
                default:
                    break;
            }
        }
        [mon sortUsingSelector:@selector(compareByStartTime:)];
        [tue sortUsingSelector:@selector(compareByStartTime:)];
        [wed sortUsingSelector:@selector(compareByStartTime:)];
        [thu sortUsingSelector:@selector(compareByStartTime:)];
        [fri sortUsingSelector:@selector(compareByStartTime:)];
        [sat sortUsingSelector:@selector(compareByStartTime:)];
        [sun sortUsingSelector:@selector(compareByStartTime:)];
        
        [group_classes setObject:mon forKey:[NSNumber numberWithInt:0]];
        [group_classes setObject:tue forKey:[NSNumber numberWithInt:1]];
        [group_classes setObject:wed forKey:[NSNumber numberWithInt:2]];
        [group_classes setObject:thu forKey:[NSNumber numberWithInt:3]];
        [group_classes setObject:fri forKey:[NSNumber numberWithInt:4]];
        [group_classes setObject:sat forKey:[NSNumber numberWithInt:5]];
        [group_classes setObject:sun forKey:[NSNumber numberWithInt:6]];
        
        schedule.group_classes = group_classes;
    }
    return schedule;
}
+ (GroupClass *) GroupClassFromDictionary:(NSDictionary *) dictionary{
    NSDateFormatter *timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [timeFormatter setDateFormat:@"hh:mm a"];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [timeFormatter setLocale:locale];
    [locale release];
    

    GroupClass *groupClass  = [[[GroupClass alloc] init] autorelease];
    
    groupClass.group_class_id = [dictionary objectForKey:@"id"];
    groupClass.name = [dictionary objectForKey:@"name"];
    groupClass.icon = [dictionary objectForKey:@"icon"];
    groupClass.start_time = [timeFormatter dateFromString:[dictionary objectForKey:@"start_time"]];
    groupClass.duration = [dictionary objectForKey:@"duration"] ;
    groupClass.day_of_week = [dictionary objectForKey:@"day_of_week"];
    groupClass.day_of_week_desc = [dictionary objectForKey:@"day_of_week_desc"];

    // combine date and time
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents *dateComponents = [calendar components:unitFlagsDate fromDate:[NSDate date]];
    unsigned unitFlagsTime = NSHourCalendarUnit | NSMinuteCalendarUnit |  NSSecondCalendarUnit;
    NSDateComponents *timeComponents = [calendar components:unitFlagsTime fromDate:groupClass.start_time];
    
    [dateComponents setSecond:[timeComponents second]];
    [dateComponents setHour:[timeComponents hour]];
    [dateComponents setMinute:[timeComponents minute]];
    
    groupClass.start_time = [calendar dateFromComponents:dateComponents];
    
    // shift by calculating diff of day of week
    // Sunday = 1, Saturday = 7
    int class_weekday = groupClass.day_of_week.intValue == 6 ? 1 : groupClass.day_of_week.intValue + 2;
    int today_weekday = [[[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:[NSDate date]] weekday];    
    int day_diff = class_weekday - today_weekday;
    groupClass.start_time = [groupClass.start_time dateByAddingTimeInterval:day_diff*24*60*60];
    
    return groupClass;
}

@end
