//
//  Schedule.m
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "Schedule.h"

@implementation Schedule

@synthesize schedule_id, name, start_date, end_date, url, is_active, is_current, group_classes;

- (void)dealloc {
    [schedule_id release];
    [name release];
	[start_date release];
	[end_date release];
    [url release];
    [group_classes release];
    [super dealloc];
}

@end
