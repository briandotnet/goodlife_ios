//
//  ServiceManager.m
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import "ServiceManager.h"

@implementation ServiceManager

#define kTimeOutSeconds      20

static ServiceManager *shared = nil;
static NSOperationQueue *queue = nil;
#pragma mark Singleton stuff

+ (ServiceManager *) sharedManager {
	
	if(!shared){
		shared = [[super allocWithZone:NULL] init];
        [ASIHTTPRequest setDefaultTimeOutSeconds:kTimeOutSeconds];
	}
	
	return shared;
}

+ (id)allocWithZone:(NSZone *)zone {
	return [[self sharedManager] retain];
}

- (NSOperationQueue *) queue
{
	if(!queue)
	{
		queue = [[NSOperationQueue alloc] init];
	}
	return queue;
}

#pragma mark - get clubs, schedules, and classes

- (void) getClubsByLocation:(CLLocationCoordinate2D) location within:(int) within sender:(id)sender{
    NSString* urlString = [NSString stringWithFormat: @"http://goodlife.heroku.com/clubs/find.json?lat=%2f&lng=%2f&within=%d", location.latitude, location.longitude, within];
    NSURL *url = [NSURL URLWithString:urlString];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.userInfo = [NSDictionary dictionaryWithObject:@"getClubsByLocation" forKey:@"request_id"];
    request.delegate = [sender retain];
    [[self queue] addOperation:request];
}
- (void) getClubsByNECoordinate:(CLLocationCoordinate2D) NECoordinate andSWCoordinate:(CLLocationCoordinate2D) SWCoordinate sender:(id)sender{
    NSString* urlString = [NSString stringWithFormat: @"http://goodlife.heroku.com//clubs/find.json?ne_lat=%2f&ne_lng=%2f&sw_lat=%2f&sw_lng=%2f", NECoordinate.latitude, NECoordinate.longitude, SWCoordinate.latitude, SWCoordinate.longitude];
    NSURL *url = [NSURL URLWithString:urlString];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.userInfo = [NSDictionary dictionaryWithObject:@"getClubsByBounds" forKey:@"request_id"];
    request.delegate = [sender retain];
    [[self queue] addOperation:request];
}
- (void) getClubsByAddress:(NSString *)address within:(int) within sender:(id)sender{
    address = [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString* urlString = [NSString stringWithFormat: @"http://goodlife.heroku.com/clubs/find.json?address=%@&within=%d", address, within];
    NSURL *url = [NSURL URLWithString:urlString];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.userInfo = [NSDictionary dictionaryWithObject:@"getClubsByAddress" forKey:@"request_id"];
    request.delegate = [sender retain];
    [[self queue] addOperation:request];
}

- (void) getSchedules:(int)clubId sender:(id)sender{
    NSString* urlString = [NSString stringWithFormat: @"http://goodlife.heroku.com/schedules/find.json?club_id=%d", clubId];
    NSURL *url = [NSURL URLWithString:urlString];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.userInfo = [NSDictionary dictionaryWithObject:@"getSchedules" forKey:@"request_id"];
    request.delegate = [sender retain];
    [[self queue] addOperation:request];
}

- (void) getSchedule:(int)scheduleId sender:(id)sender{
    NSString* urlString = [NSString stringWithFormat: @"http://goodlife.heroku.com/schedules/%d.json", scheduleId];
    NSURL *url = [NSURL URLWithString:urlString];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.userInfo = [NSDictionary dictionaryWithObject:@"getGroupClasses" forKey:@"request_id"];
    request.delegate = [sender retain];
    [[self queue] addOperation:request];
}

-(void) postFeedback:(NSDictionary *)feedback sender:(id) sender{
    NSString* urlString = @"http://goodlife.heroku.com/feedbacks.json";
    NSURL *url = [NSURL URLWithString:urlString];
    SBJsonWriter *json;
    json = [[[SBJsonWriter alloc] init] autorelease];
    NSDictionary *paddedFeedback = [NSDictionary dictionaryWithObject:feedback forKey:@"feedback"];
    NSString *feedbackString = [json stringWithObject:paddedFeedback];
    NSLog(@"%@",feedbackString);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request appendPostData:[feedbackString dataUsingEncoding:NSUTF8StringEncoding]];
    request.userInfo = [NSDictionary dictionaryWithObject:@"postFeedback" forKey:@"request_id"];
    request.delegate = [sender retain];
    [[self queue] addOperation:request];

}

@end
