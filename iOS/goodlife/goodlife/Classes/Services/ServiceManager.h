//
//  ServiceManager.h
//  goodlife
//
//  Created by Brian Ge on 12-01-20.
//  Copyright (c) 2012 T4G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "SBJson.h"
#import "Club.h"
#import "Schedule.h"
#import "GroupClass.h"

@interface ServiceManager : NSObject{

}

+ (ServiceManager *) sharedManager;

- (void) getClubsByLocation:(CLLocationCoordinate2D) location within:(int) within sender:(id)sender;
- (void) getClubsByNECoordinate:(CLLocationCoordinate2D) NECoordinate andSWCoordinate:(CLLocationCoordinate2D) SWCoordinate sender:(id)sender;
- (void) getClubsByAddress:(NSString *)address within:(int) within sender:(id)sender;
- (void) getSchedules:(int)clubId sender:(id)sender;
- (void) getSchedule:(int)scheduleId sender:(id)sender;
-(void) postFeedback:(NSDictionary *)feedback sender:(id) sender;
@end
